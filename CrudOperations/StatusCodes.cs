﻿namespace CrudOperations
{
    public static class StatusCodes
    {
        public const int HTTP_OK = 200;
        public const int HTTP_CREATED = 201;
        public const int HTTP_DATANOTMATCHED = 204;
        public const int HTTP_INTERNAL_SERVER_ERROR = 400;
        public const int HTTP_UNAUTHORIZED = 401;
        public const int HTTP_FORBIDDEN = 403;
        public const int HTTP_NOT_FOUND = 403;
        public const int HTTP_ALREADYEXISTS = 409;
        public const int HTTP_LOCKED = 423;
    }
}