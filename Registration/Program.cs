    using AuthLibrary;
    using CommonComponent.Email;
    using CommonComponent.Message;
    using CrudOperations;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.DataProtection.KeyManagement;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;
    using Registration.Infrastructure;
    using Registration.Core.Authentication;
    using Registration.Core.DataProtector;
    using Registration.Core.Interface;
    using System.Globalization;
    using System.Text;
    using Microsoft.AspNetCore.DataProtection;

    var builder = WebApplication.CreateBuilder(args);
    var configuration = builder.Configuration;
    var connectionString = builder.Configuration.GetConnectionString("Registration") ?? throw new InvalidOperationException("Connection string 'Registration' not found.");

    


// Add services to the container.

builder.Services.AddControllers();


    //Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "JWTToken_Auth_API",
            Version = "v1"
        });
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
        {
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement {
            {
                new OpenApiSecurityScheme {
                    Reference = new OpenApiReference {
                        Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                    }
                },
                Array.Empty<string>()
            }
        });
    });


    IWebHostEnvironment _environment = builder.Environment;
    //app.Environment;

    var keyFolder = Path.Combine(_environment.ContentRootPath, "key");

    builder.Services.AddDataProtection()
        .PersistKeysToFileSystem(new DirectoryInfo(keyFolder)); ;

    //Interface dependency
    builder.Services.AddSingleton<IAuthLib>(x => new DataAccess(x.GetService<IConfiguration>(), connectionString));
    builder.Services.AddScoped<ICrudOperationService>(x => new CrudOperationDataAccess(x.GetService<IConfiguration>(), connectionString));
    builder.Services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
    builder.Services.AddScoped<IRegistrationRepository, RegistrationRepository>();
    builder.Services.AddScoped<Registration.Filter.ValidationFilter>();
    builder.Services.AddScoped<IDataProtectionRepository, DataProtectionRepository>();




    //JWT Authentication
    var key = Encoding.ASCII.GetBytes(configuration["SecretKey"]!);
    builder.Services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(options => {
        options.Events = new JwtBearerEvents
        {
            OnTokenValidated = context =>
            {
                return Task.CompletedTask;
            }
        };
        options.RequireHttpsMetadata = false;
        options.SaveToken = true;

        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false,
            ClockSkew = TimeSpan.Zero
        };
    });


    builder.Services.AddScoped<IEmailService>(x =>
    {
        return new EmailService(new EmailConfig
        {
            /*
             * uncomment below line of code to add procedure for fetching content from database
             * 
             *emailcontentspname = "[account].[uspemailtemplatecontentlist]", 
             *  
            */
            Username = builder?.Configuration["AppSettings:Smtp:UserName"] ?? "",
            Password = builder?.Configuration["AppSettings:Smtp:Password"] ?? "",
            EnableSSL = Convert.ToBoolean(builder?.Configuration["AppSettings:Smtp:EnableSSl"], CultureInfo.CurrentCulture),
            FromEmail = builder?.Configuration["AppSettings:Smtp:From"] ?? "",
            Host = builder?.Configuration["AppSettings:Smtp:Host"] ?? "",
            Port = Convert.ToInt32(builder?.Configuration["AppSettings:Smtp:Port"], CultureInfo.CurrentCulture)
        }, connectionString);



    });


    builder.Services.AddScoped<ISMSService>(x => new SMSService(new MessageConfig
    {
        AccountSID = (builder?.Configuration["TwilioSettings:AcccountSid"] ?? ""),
        TwilioAuthToken = (builder?.Configuration["TwilioSettings:Authtoken"] ?? ""),
        FromNumber = (builder?.Configuration["TwilioSettings:CallerPhoneNumber"] ?? "")
    }));


    //Cors Policy
    builder.Services.AddCors(options =>
    {
        options.AddPolicy("AllowLocalhost4200",
            builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            });
    });

//static void ConfigureServices(IServiceCollection services)
//{
//    services.AddCors(options =>
//    {
//        options.AddPolicy("AllowLocalhost4200",
//            builder =>
//            {
//                builder.WithOrigins("http://localhost:4200")
//                       .AllowAnyHeader(*)
//                       .AllowAnyMethod();
//            });
//    });

//    // Other services configuration...
//}


//static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
//{
//    // ...

//    app.UseCors("AllowLocalhost4200");

//    // Other middleware configurations...
//}




var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();
    app.UseCors("AllowLocalhost4200");
    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();



    builder.Host.ConfigureAppConfiguration((ctx, builder) =>
    {
        var enviroment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        builder.AddJsonFile("appsettings.json", false, true);
        builder.AddJsonFile($"appsettings.{enviroment}.json", true, true);
        builder.AddEnvironmentVariables();
    });

    
