﻿using Microsoft.AspNetCore.Mvc;
using Registration.Core.Authentication;
using Registration.Core.DataProtector;
using Registration.Filter;
using AuthLibrary;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using Registration.Infrastructure;
using CommonComponent.Email;
using System.Security.Principal;
using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using CommonComponent.Storage;
using CommonComponent.Message;
using Registration.INFRASTRUCTURE.InfraHelper;
using Registration.Core.Common;
using CrudOperations;

namespace Registration.Controllers
{
    [Route("api/Authentication"), ServiceFilter(typeof(ValidationFilter))]
    public class AuthenticationController : Controller
    {

        private readonly IAuthenticationRepository _authenticate;
        private readonly IAuthLib _authLib;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService; 
        private readonly ISMSService _service;

         





        public AuthenticationController(IAuthenticationRepository authenticate, IAuthLib authLib, IConfiguration configuration, 
            IEmailService emailService, ISMSService service)
        {
            _authenticate = authenticate;
            _authLib = authLib;
            _configuration = configuration;
            _emailService = emailService;
            _service = service;
        }



        [HttpPost("signUp")]
        public async Task<IActionResult> UserCreation([FromBody] DtoUserCreationParameters? dtoUserCreationParameters)
        {
           //dtoUserCreationParameters ??= new();
           // dtoUserCreationParameters.CreatedBy = new Guid(string.IsNullOrEmpty(User?.Identity?.Name) ? "D91D070D-59E9-4B66-B856-66A8223391DD" : User?.Identity?.Name!);
            //dtoUserCreationParameters.CreatedBy = new Guid(User?.Identity?.Name!);
            var response = await _authenticate.UserCreation(dtoUserCreationParameters!).ConfigureAwait(false);
            if (response.Status)
            {
                return StatusCode(StatusCodes.Status201Created, response);
            }
            else if (!response.Status)
            {
                return StatusCode(StatusCodes.Status409Conflict, response);
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, "Something went wrong, Please contact system administrator");
            }
        }


        [HttpPost("login")]
        public async Task<IActionResult> UserLogin([FromBody] DtoUserLoginParemeter dtoUserLoginParemeter)
        {
            var response = await _authenticate.UserLogin(dtoUserLoginParemeter).ConfigureAwait(false);
            var userInformationDetails = response.Data;
            if (response.Status)
            {
                if (userInformationDetails.IsTwoFactorAuthenticationRequired && !string.IsNullOrEmpty(userInformationDetails.Email))
                {
                    string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);


                    //string phone = userInformationDetails.Phone;
                    //await _service.SendSMS(phone, string.Format(CultureInfo.InvariantCulture, "OTP for your login is {0}.The OTP expires within 3 min", otp)).ConfigureAwait(false);
                    var otpSaveResponse = await _authLib.SaveToken("[uspUserSaveOtp]", new
                    {
                        otp,
                        userInformationDetails.UserId,

                        
                    }).ConfigureAwait(false);


                    if (otpSaveResponse.Status)
                    {
                        //string phone = userInformationDetails.Contact!.Replace("-", "");
                        //var sms = await _service.SendSMS(phone, string.Format(CultureInfo.InvariantCulture, "OTP for your login is {0}.The OTP expires within 3 min", otp)).ConfigureAwait(false);

                        string body;
                        using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                        {
                            body = reader.ReadToEnd();
                        }

                        ////CompanyName
                        ////string CompanyName = ;

                        //UserDetailsGetParam userDetailsGetParam = new UserDetailsGetParam()
                        //{
                        //    Email = dtoLogin?.Email
                        //};

                        //Response<UserDetails> userDetailsResponse = await _account.GetUserBasicDetailsAsync(userDetailsGetParam).ConfigureAwait(false);


                        //string? companyName = _configuration.GetValue<string>("CompanyName");
                        string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                        body = body.Replace("{{otp}}", otp)
                            .Replace("{{username}}", userInformationDetails.Name)
                            .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyNameMailSender"))
                            .Replace("{{adminemail}}", adminEmail);

                        string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                        _emailService?.SendEmail(dtoUserLoginParemeter?.Email ?? "", mailSubject, body, true);


                        return Ok(new
                        {
                            userInformationDetails.UserId,
                            adminEmail,
                            userInformationDetails.IsTwoFactorAuthenticationRequired,
                            otpSaveResponse.Message,
                            LoginStatus = response.Message
                        });
                    }
                    else
                    {
                        return BadRequest(new
                        {
                            message = otpSaveResponse.Message
                        });
                    }
                }
                else
                {
                    //if (!string.IsNullOrWhiteSpace(userInformationDetails.ProfileImage))
                    //{
                    //    userInformationDetails.ProfileImage = ""; //await _storageService.Download(userInformationDetails.ProfileImage, 24 * 60); // 1 day because it will show in header?
                    //}

                    string? token = await _authLib.GenerateJWTToken(userInformationDetails?.UserId!.ToString(), userInformationDetails?.Email, userInformationDetails?.Role, expiration: DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);

                    await _authLib.SaveToken("[dbo].[uspUserTokenSave]", new
                    {
                        userInformationDetails?.UserId,
                        token
                        //dtoLogin.DeviceToken,
                        //dtoLogin.DevicePlatform
                    }).ConfigureAwait(false);

                    if (!response.Status)
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, "Something went wrong, Please contact system administrator");
                    }

                    return Ok(new
                    {
                        status = true,
                        message = "successfully logged into the system",
                        data = new
                        {
                            userInformationDetails?.UserId,
                            userInformationDetails?.Email,
                            userInformationDetails?.Name,
                            userInformationDetails?.Role,
                            LoginStatus = response.Message
                        },
                        token
                    });
                }
            }
            else if (!response.Status && response.Message == "DAU" || response.Message == "DEU" || response.Message == "ACL")
            {

                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                return StatusCode(StatusCodes.Status403Forbidden, response);
            }
            else if (!response.Status && response.Message == "UNF")
            {
                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                return StatusCode(StatusCodes.Status404NotFound, response);
            }
            else if (!response.Status && response.Message.Trim() == "ALK")
            {
                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                return StatusCode(StatusCodes.Status404NotFound, response);
            }
            else if (!response.Status && (response.Message.Trim() == "TPX"))
            {
                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", response.Message.Trim().ToLower(CultureInfo.CurrentCulture));
                return StatusCode(StatusCodes.Status404NotFound, response);
            }
            else if (!response.Status && response.Message.Trim() == "Invalid password")
            {
                DtoInvalidAttemptChecker checker = new()
                {
                    Email = dtoUserLoginParemeter?.Email
                };

                var responseInvalidAttempt = await _authenticate.InvalidLoginAttemptChecker(checker).ConfigureAwait(false);

                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "invalid-credential"); ;
                //invalid-credential

                if (responseInvalidAttempt.Message == "ACL")
                {


                    var invalidResponseData = responseInvalidAttempt.Data;

                    string body;
                    using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "AccountLockedMail.html")))
                    {
                        body = reader.ReadToEnd();
                    }

                    //string? userFirstName = invalidResponseData.Name;
                    //string? invalidAttemptCount = Convert.ToString(invalidResponseData.InvalidAttemptAllowed, CultureInfo.CurrentCulture);

                    //body = body.Replace("{{FIRSTNAME}}", userFirstName)
                    //    .Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"))
                    //    .Replace("{{invalid_attempt}}}", invalidAttemptCount);

                    //string mailSubject = EmailSubjectHelper.AccountLockSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                    //_emailService?.SendEmail(dtoUserLoginParemeter?.Email ?? "", mailSubject, body, true);

                }

                return StatusCode(StatusCodes.Status404NotFound, response);
            }
            else
            {
                response.Message = "Something went wrong, Please contact system administrator";
                return StatusCode(StatusCodes.Status400BadRequest, response);
            }

        }


        [HttpGet, Route("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            var response = await _authLib.Logout(HttpContext.Request.Headers["Authorization"].FirstOrDefault(), "[uspUserLogout]", new
            {
                UserId = User?.Identity?.Name,

            }).ConfigureAwait(false);

            if (!response.Status)
            {
                return BadRequest(response);
            }

            return Ok(response);
        }


        //---------------   OTP Section   ------------------
        [AllowAnonymous, HttpPost, Route("otp/verify")]
        public async Task<IActionResult> VerifyOTP([FromBody] DtoVerifyOtp verifyOtp)
        {

            var verifyOtpResponse = await _authenticate.VerifyOtpAsync(verifyOtp).ConfigureAwait(false);

            return StatusCode(
                (!verifyOtpResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    Response = verifyOtpResponse,

                    verifyOtpResponse.Data?.Token
                });

        }


        [AllowAnonymous, HttpPost, Route("otp/resend")]
        public async Task<IActionResult> ResendOtp([FromBody] DtoOtpDetails dtoOtpDetails)
        {
            var otpSaveResponse = await _authenticate.ResendOtpAsync(dtoOtpDetails).ConfigureAwait(false);

            return StatusCode(
                (!otpSaveResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    Response = otpSaveResponse

                });
        }



        //----------------  Forget password & Generate link token   -----------------
        [HttpPost, Route("password/forgot")]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] DtoForgotPasswordRequest forgot)
        {

            var forgotPasswordResponse = await _authenticate.ForgotPasswordVerifyAsync(forgot).ConfigureAwait(false);

            return StatusCode(
                (!forgotPasswordResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    Response = forgotPasswordResponse
                });

        }

        [HttpPost, Route("password/verifytoken")]
        public async Task<IActionResult> ForgotPasswordVerifyTokenAsync([FromBody] DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {

            var forgotPasswordResponse = await _authenticate.VerifyPasswordTokenAsync(passwordVerifyTokenParam).ConfigureAwait(false);

            return StatusCode(
                (!forgotPasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK),
                new
                {
                    Response = forgotPasswordResponse
                });

        }

        [HttpPost, Route("password/reset")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] DtoResetPasswordParam resetPasswordParam)
        {
            //resetPasswordParam.UpdatedBy = Guid.Parse(User?.Identity?.Name ?? "");
            resetPasswordParam ??= new();
            resetPasswordParam.IsEncrypted = true;
            resetPasswordParam.ScreenName = FlagHelper.ResetPasswordScreen;

            var resetPasswordResponse = await _authenticate.ResetPasswordAsync(resetPasswordParam).ConfigureAwait(false);

            return StatusCode(
                (!resetPasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK),
                new
                {
                    Response = resetPasswordResponse
                });

        }

        //---------------   Change password    ------------------
        [Authorize, HttpPost, Route("password/changePassword")]
        public async Task<IActionResult> ChangeUserPasswordAsync([FromBody] DtoChangePasswordRequestParam changePasswordRequestParam)
        {
            changePasswordRequestParam ??= new DtoChangePasswordRequestParam();
            changePasswordRequestParam.UpdatedBy = Guid.Parse(User?.Identity?.Name!);
            changePasswordRequestParam.ScreenName = FlagHelper.ChangePasswordScreen;
            var chanePasswordResponse = await _authenticate.ChangePasswordAsync(changePasswordRequestParam).ConfigureAwait(false);

            return StatusCode(
                chanePasswordResponse.Status ? StatusCodes.Status200OK : RepoCommon.GetStatusCodeBykey(chanePasswordResponse.Data),
                new { Response = chanePasswordResponse });

        }


        //---------------   Drop Down Menu   -----------------
        [HttpGet("dropDown")]
        public async Task<IActionResult> GetDropdownData(DtoDropDownTypeReq dtoDropDownTypeReq)    
        {
            ResponseList<DtoMasterDropdownList> response = new();
            if (string.IsNullOrWhiteSpace(dtoDropDownTypeReq.DepartmentId) && string.IsNullOrWhiteSpace(dtoDropDownTypeReq.SalaryId))
            {
                response.Message = "Enter Atleast one input, Don't leave empty both";
                return StatusCode(StatusCodes.Status400BadRequest, response);
            }

            //DtoDropDownTypeReq param = new()
            //{
            //    DepartmentId = departmentId,
            //    SalaryId = salaryId
            //};
            response = await _authenticate.GetDropDownMasterAsync(dtoDropDownTypeReq).ConfigureAwait(false);
            if (!response.Status)
            {   
                //response.Message = "Please provide valid type";
                return StatusCode(StatusCodes.Status400BadRequest, response);
            }

            //if (departmentId == "Department")
            //{
            //    response = await _authenticate.GetDropDownMasterAsync(param).ConfigureAwait(false);
            //}
            //else if (departmentId == "Salary")
            //{
            //    response = await _authenticate.GetDropDownMasterAsync(param).ConfigureAwait(false);
            //}
            //else
            //{
            //    response.Message = "Please provide valid type";
            //    return StatusCode(StatusCodes.Status400BadRequest, response);
            //}

            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status500InternalServerError, response);
        }
    }
}
