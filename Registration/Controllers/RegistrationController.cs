﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Registration.Core.Interface;
using Registration.Core.Models;
using System.Security.Claims;

namespace Registration.Controllers
{
    [Route("api/Registration/")]
    public class RegistrationController : Controller
    {
        private readonly IRegistrationRepository _registrationRepository;

        public RegistrationController(IRegistrationRepository registrationRepository)
        {
            _registrationRepository = registrationRepository;
        }

        [AllowAnonymous]
        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp([FromBody] SignUpParameters signUpParameters)
        {
            return Ok(await _registrationRepository.SignUp(signUpParameters));
        }

        [AllowAnonymous]
        [HttpPut("SignIn")]
        public async Task<IActionResult> SignIn([FromBody] SignInParameters signInParameters)
        {
            TokenResponse response;
            response = await _registrationRepository.SignIn(signInParameters);
            if (!response.Status)
            {
                return Ok(response);
            }
            SetTokenValidOrInvalid setTokenValidOrInvalid = new()
            {
                Email = signInParameters.Email,
                Token = response.Token,
                SetTokenValid = response.Status
            };
            var result = await _registrationRepository.SetTokenVaild(setTokenValidOrInvalid);
            if (!result.Status)
            {
                return Ok(result);
            }
            return Ok(response);
        }


        [HttpPatch("SignOut")]
        [Authorize]
        public async Task<IActionResult> LogoutAsync()
        {
            SetTokenValidOrInvalid logOutParemeter = new()
            {
                Email = User.FindFirst(ClaimTypes.Email)!.Value,
                Token = (HttpContext.Request.Headers["Authorization"].FirstOrDefault()!)["Bearer ".Length..],
                SetTokenValid = false
            };
            return Ok(await _registrationRepository.LogOut(logOutParemeter));

            // Return a success response
            //return Ok("Logged out successfully");
        }


        [Authorize(Roles ="HR"), HttpPatch("SetRole")]
        public async Task<IActionResult> SetRole([FromBody] SetRoleParemeter setRoleParemeter)
        {
            Response response;
            UserTokenInfo userTokenInfo = new()
            {
                Email = User.FindFirst(ClaimTypes.Email)!.Value,
                Token = (HttpContext.Request.Headers["Authorization"].FirstOrDefault()!)["Bearer ".Length..]
            };

            response = await _registrationRepository.CheckTokenValid(userTokenInfo);
            if (!response.Status)
            {
                return Ok(response);
            }
            return Ok(await _registrationRepository.SetRole(setRoleParemeter));
        }

    }
}
