﻿using Registration.Infrastructure;
using System.Text.Json;
using System.Net;
using Registration.Core.Common;
using System.Globalization;

namespace Registration.Services.ErrorLogger
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;



        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }



        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context?.Response;



                if (response != null)
                {
                    response.ContentType = "application/json";
                    response.StatusCode = error switch
                    {
                        NullReferenceException _ => (int)HttpStatusCode.InternalServerError,
                        KeyNotFoundException _ => (int)HttpStatusCode.NotFound,
                        UnauthorizedAccessException _ => (int)HttpStatusCode.Unauthorized,
                        InvalidOperationException _ => (int)HttpStatusCode.BadRequest,
                        // Add More exception types here
                        _ => (int)HttpStatusCode.InternalServerError,
                    };



                    string resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "internal-server-error-message") ?? "";



                    ResponseObj responseModel = new() { Status = false, Message = error.Message };
                    if (response.StatusCode == (int)HttpStatusCode.InternalServerError)
                        responseModel.Message = $"{responseModel.Message} {resultMessage}";


                    var result = JsonSerializer.Serialize(responseModel);
                    _ = Convert.ToString(context?.Request.RouteValues["controller"], CultureInfo.CurrentCulture);
                    _ = Convert.ToString(context?.Request?.RouteValues["action"], CultureInfo.CurrentCulture);

                    //await Logger.ErrorAsync((controller ?? ""), (action ?? ""), (context?.User?.Identity?.Name ?? "F1673AE9-D195-4DAC-A516-46F56DE6791E"), error
                    //    //_dataProtectionService
                    //    ).ConfigureAwait(false);

                    if (result != null)
                        await response.WriteAsync(result).ConfigureAwait(false);
                }

            }
        }
    }
}
