﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Registration.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Interface
{
    public interface IRegistrationRepository
    {
        Task<Response> SignUp(SignUpParameters signUpParameter);
        Task<TokenResponse> SignIn(SignInParameters signInParameters);
        Task<Response> LogOut(SetTokenValidOrInvalid logOutParemeter);
        Task<Response> SetRole(SetRoleParemeter setRoleParemeter);
        Task<Response> CheckTokenValid(UserTokenInfo userTokenInfo);
        Task<Response> SetTokenVaild(SetTokenValidOrInvalid setTokenValidOrInvalid);
    }
}
