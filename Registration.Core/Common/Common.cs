﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Common
{
    public class DataTableParams
    {
        public string? SearchKey { get; set; }

        [Required]
        public int Start { get; set; }

        [Required]
        public int PageSize { get; set; }

        public string? SortCol { get; set; }
        public string? Draw { get; set; }
    }
    public class Appsettings
    {
        public string? Secret { get; set; }
        public Smtp? Smtp { get; set; }
    }

    public class Smtp
    {
        public string? Host { get; set; }
        public string? Port { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }

        //public bool enableSSl { get; set; }
        public string? EnableSSl { get; set; }

        public string? From { get; set; }
    }


    public class ResponseObj
    {
        public bool? Status { get; set; }
        public string? Message { get; set; }

        public string? FileDownloadName { get; set; }
    }

    public class DownloadDataParam
    {
        public string? FileType { get; set; }
        public string? Module { get; set; }
    }

    public class ResponseListObject
    {
        public bool Status { get; set; }
        public string? Message { get; set; }
        public object? Data { get; set; }
        public long? TotalRecords { get; set; } = 0;
    }


    #region Download

    public class DtoDownloadDataParam : DataTableParams
    {

        public Guid? Id { get; set; }

    }

    //public class DtoListingCodeParams
    //{
    //    public string? SearchKey { get; set; }
    //    public int? Start { get; set; }
    //    public int? PageSize { get; set; }
    //    public string? SortCol { get; set; }
    //}

    //public class DtoCodesData
    //{
    //    public string? ProcedureCode { get; set; }
    //    public string? Description { get; set; }
    //    public bool? IsActive { get; set; }
    //}

    //public class DtoDiagnosisCodesData
    //{
    //    public string? DiagnosisCode { get; set; }
    //    public string? Description { get; set; }
    //    public bool? IsActive { get; set; }

    //}

    //public class DtoPlaceOfServiceData
    //{
    //    public string? ServiceCode { get; set; }
    //    public string? Service { get; set; }
    //    public string? Description { get; set; }
    //    public bool? IsActive { get; set; }

    //}

    //public class DtoTypeOfServiceData
    //{
    //    public string? ServiceCode { get; set; }
    //    public string? Service { get; set; }
    //    public bool? IsActive { get; set; }

    //}

    public class DtoModifiersData
    {
        public string? Modifier { get; set; }
        public string? Description { get; set; }
        public bool? IsActive { get; set; }

    }

    public class DtoProcedureCategoryCodesData
    {
        public string? CategoryCode { get; set; }
        public string? Category { get; set; }
        public bool? IsActive { get; set; }

    }

    public class DtoDenialCategoryDownloadData
    {

        public string? Denial_Category_Code { get; set; }
        public string? Denial_Category_Description { get; set; }
        public bool IsActive { get; set; }

    }

    public class DtoVisitFieldsData
    {
        public string? FieldName { get; set; }
        public string? FieldCategory { get; set; }
        public string? InputType { get; set; }
        public bool? IsActive { get; set; }

    }

    public class DtoPatientFieldsData
    {
        public string? FieldName { get; set; }
        public string? FieldCategory { get; set; }
        public string? InputType { get; set; }
        public bool? IsActive { get; set; }

    }

    public class DtoScrubbingRuleErrorCategoryDownloadData
    {

        public string? Category_Name { get; set; }
        public string? Category_Description { get; set; }
        public bool IsActive { get; set; }

    }
    public class DtoDropDownTypeReq
    {
        public string? DepartmentId { get; set; }
        public string? SalaryId { get; set; }
    }

    public class DtoMasterDropdownList
    {
        public string? UserId { get; set; }
        public string? Name { get; set; }
        public string? Department { get; set; }
        public double? Income { get; set; }
    }
    #endregion Download
}




