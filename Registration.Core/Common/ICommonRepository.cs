﻿using AuthLibrary;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Common
{
    public interface ICommonRepository
    {

        string GenerateToken();

    }
}
