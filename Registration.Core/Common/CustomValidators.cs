﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Registration.CORE.Common
{
    public class DenyHtmlInputAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success!;

            var tagWithoutClosingRegex = new Regex(@"<[^>]+>");

            var hasTags = tagWithoutClosingRegex.IsMatch(value.ToString()!);

            if (!hasTags)
                return ValidationResult.Success!;

            return new ValidationResult(String.Format("{0} HTML tags not allowed", validationContext.DisplayName));
        }
    }

    public class StringLengthValidator : StringLengthAttribute
    {
        public readonly string _propertyName;

        public StringLengthValidator(int maximumLength, string? propertyName = null) : base(maximumLength)
        {
            _propertyName =  propertyName!;
        }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            var result = base.IsValid(value, validationContext);
            if (result != null)
                result.ErrorMessage = string.IsNullOrWhiteSpace(result.ErrorMessage) ? "" : string.Format("The {0} value cannot exceed {1} characters.", _propertyName ?? validationContext.MemberName, MaximumLength);
            return result!;
        }
    }

    public class EmailInputAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()))
                return ValidationResult.Success!;

            var tagWithoutClosingRegex = new Regex(@"\A(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?-i)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");

            var hasTags = tagWithoutClosingRegex.IsMatch(value.ToString()!);

            if (hasTags)
            {
                return ValidationResult.Success!;
            }
            else
            {
                return new ValidationResult(String.Format("The {0} is not a valid e-mail address.", value));
            }
        }
    }

    public class PasswordInputAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success!;

            var tagWithoutClosingRegex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,}$");

            var hasTags = tagWithoutClosingRegex.IsMatch(value.ToString()!);

            if (hasTags)
            {
                return ValidationResult.Success!;
            }
            else
            {
                return new ValidationResult("Password must be 8 Characters long and must contain at least one upper case character, one lowercase character, one number and one special character.");
            }
        }
    }

    public class PhoneInputAttribute : ValidationAttribute
    {
        private readonly string _elementName;

        public PhoneInputAttribute(string ElementName = "Phone")
        {
            _elementName = ElementName;
        }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()))
                return ValidationResult.Success!;

            var tagWithoutClosingRegex = new Regex(@"^\d{3}-\d{3}-\d{4}$");

            var hasTags = tagWithoutClosingRegex.IsMatch(value.ToString()!);

            if (hasTags)
            {
                return ValidationResult.Success!;
            }
            else
            {
                return new ValidationResult(String.Format("The {0} is Not a valid {1} number. {1} Number must have a format as 000-000-0000.", value, _elementName));
            }
        }
    }

    public class UniqueIdInputAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success!;

            var tagWithoutClosingRegex = new Regex(@"^[0-9]{6}$");

            var hasTags = tagWithoutClosingRegex.IsMatch(value.ToString()!);

            if (hasTags)
            {
                return ValidationResult.Success!;
            }
            else
            {
                return new ValidationResult(String.Format("The {0} is Not a valid unique id. The unique id must be 6 digit number.", value));
            }
        }
    }

    #region File Upload

    public sealed class MaxFileSize : ValidationAttribute
    {
        public int MaxSize { get; set; }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            var list = value as IList;
            if (list != null)
            {
                foreach (var item in list)
                {
                    var file = item as IFormFile;
                    if (file!.Length > MaxSize)
                    {
                        //return false;
                        return new ValidationResult($"'{file.FileName}' should be less then {MaxSize / 1024 / 1024}MB.");
                    }
                }
            }
            //return true;
            return ValidationResult.Success!;
        }
    }

    public sealed class ValidFileType : ValidationAttribute
    {
        public string[]? Extensions { get; set; }

        protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
        {
            var list = value as IList;
            if (list != null)
            {
                foreach (var item in list)
                {
                    var file = item as IFormFile;
                    var extension = "." + file!.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                    var pos = Array.IndexOf(Extensions!, extension);
                    if (pos == -1)
                    {
                        return new ValidationResult($"'{file.FileName}' is not a valid file type. Only {String.Join(", ", Extensions!)} file types are allowed.");
                    }
                }
            }
            return ValidationResult.Success!;
        }
    }

    #endregion File Upload
}