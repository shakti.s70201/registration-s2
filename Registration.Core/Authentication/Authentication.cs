﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Authentication
{

    public class DtoUserCreationParameters
    {
        //public Guid? UserId { get; set; }


        [Required, StringLength(40, ErrorMessage = "The Name value cannot exceed 40 characters. "),
         RegularExpression("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)",
         ErrorMessage = "Valid Charactors include (A-Z) (a-z) (' space -)")]
        public string? Name { get; set; }


        public string? Gender { get; set; }

        [Required, StringLength(13, ErrorMessage = "The Mobile value cannot exceed 13 characters. ")]
        public string? Contact { get; set; }


        [Required, EmailAddress]
        public string? Email { get; set; }


        [DataType(DataType.Date)]
        [Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public string? DOB { get; set; }


        public string? Role { get; set; } = "User";

        public bool IsTwoFactorAuthenticationRequired { get; set; } = false;


        [Required, DataType(DataType.Password)]
        public string? Password { get; set; }

        public Guid? CreatedBy { get; set; }

        //public byte[]? PasswordHash { get; set; }
        //public byte[]? PasswordSalt { get; set; }
    }

    public class DtoUserLoginParemeter
    {
        [Required]
        [RegularExpression(@"\A(?i)(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?-i)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
         ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string? Email
        {
            get; set;
        }

        [Required, DataType(DataType.Password)]
        public string? Password
        {
            get; set;
        }
    }



    //----------   Getting user detail   ------------------
    public class UserDetailsGetParameters
    {
        public string? UserId { get; set; }
        public string? Email { get; set; } = string.Empty;
        public bool IsUserActive { get; set; }
    }

    public class UserInformationDetails
    {
        public string? UserId { get; set; }
        public string? Name { get; set; }
        public string? Gender { get; set; }
        public string? Contact { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; }
        public string? DOB { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public bool IsTwoFactorAuthenticationRequired { get; set; }
        public string? Token { get; set; }
    }


    //----------    Invalid Attempt parameters   ----------
    public class DtoInvalidAttemptResponse
    {
        public string? Name { get; set; } = string.Empty;
        public int? InvalidAttemptAllowed { get; set; }
    }
    public class DtoInvalidAttemptChecker
    {
        [Required, EmailAddress, StringLength(150, ErrorMessage = "email should be less than or equal to 150 characters.")]
        public string? Email
        {
            get; set;
        }
    }


    //----------    OTP varification parameters    --------
    public class DtoVerifyOtp
    {
        [Required(AllowEmptyStrings = false), StringLength(6)]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "Otp should be numerical only")]
        public string? Otp { get; set; }

        [Required]
        public string? UserId { get; set; }

        //public string? DeviceToken { get; set; }

        //public string? DevicePlatform { get; set; }

        //public string? DeviceId { get; set; }

    }


    //----------    Forget & verify   --------------------
    public class DtoForgotPasswordRequest
    {
        [Required, EmailAddress]
        public string? Email { get; set; }
    }
    public class DtoForgotPassword
    {
        public string? Link { get; set; }
        public string? Email { get; set; }
        public string? EmailToken { get; set; }
    }

    public class DtoPasswordVerifyTokenParam
    {
        public string? EmailId { get; set; }
        public string? Token { get; set; }
    }
    public class DtoUserVerifyDetails
    {
        public string? UserId { get; set; }
        public string? EmailId { get; set; }

    }



    //----------   for saved OTP & UserId into database   ---------
    public class DtoSaveOtpParam
    {
        public string? Otp { get; set; }

        public string? UserId { get; set; }
    }
    public class DtoOtpDetails
    {
        public string? UserId { get; set; }
        public string? AdminEmail { get; set; }
        public bool? IsTwoFactorAuthenticationRequired { get; set; }
    }



    //----------    Reset Paassword     ---------------------------
    public class DtoResetPasswordParam
    {
        public string? Email { get; set; }
        public string? EmailToken { get; set; }
        public string? UserId { get; set; }
        public string? Password { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public string? UpdatedBy { get; set; }
        public string? Opr { get; set; }
        public bool IsEncrypted { get; set; }
        public string? ScreenName { get; set; }
    }
    public class DtoUserOldPasswords
    {
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
    }



    //----------    Changed Password    --------------------------
    public class DtoChangePasswordRequestParam
    {
        public string? UserId { get; set; }
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{8,}$", ErrorMessage = "Password must be 8 Characters long and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        [Required]
        public string? Password { get; set; }
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string? ConfirmPassword { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public Guid? UpdatedBy { get; set; }
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? OldPassword { get; set; }
        public string? ScreenName { get; set; }
        public string? Opr { get; set; }
        public string? EmailToken { get; set; }
    }











}
