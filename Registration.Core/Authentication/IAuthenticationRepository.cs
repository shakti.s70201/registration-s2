﻿using AuthLibrary;
using CrudOperations;
using Microsoft.AspNetCore.Mvc;
using Registration.Core.Common;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Response = CrudOperations.Response;

namespace Registration.Core.Authentication
{
    public interface IAuthenticationRepository
    {

        //Login/Logout/Verifyotp/Forgot/reset/changepassword/

        Task<Response> UserCreation(DtoUserCreationParameters dtoUserCreationParameters);

        Task<AuthResponse<UserInformationDetails>> UserLogin(DtoUserLoginParemeter dtoUserLogin);

        Task<Response<DtoInvalidAttemptResponse>> InvalidLoginAttemptChecker(DtoInvalidAttemptChecker checker);

        Task<Response<UserInformationDetails>> VerifyOtpAsync(DtoVerifyOtp verifyOtp);

        Task<Response<DtoOtpDetails>> ResendOtpAsync(DtoOtpDetails dtoOtpDetails);

        Task<Response<DtoForgotPassword>> ForgotPasswordVerifyAsync(DtoForgotPasswordRequest forgotPasswordRequest);

        Task<Response<DtoUserVerifyDetails>> VerifyPasswordTokenAsync(DtoPasswordVerifyTokenParam passwordVerifyTokenParam);

        Task<Response> ResetPasswordAsync(DtoResetPasswordParam resetPasswordParam);

        Task<Response> ChangePasswordAsync(DtoChangePasswordRequestParam changePasswordRequestParam);


        //------------------------------------------------------------------------------------------
        Task<ResponseList<DtoMasterDropdownList>> GetDropDownMasterAsync(DtoDropDownTypeReq param);


    }
}
