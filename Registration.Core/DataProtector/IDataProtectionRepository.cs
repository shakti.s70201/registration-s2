﻿namespace Registration.Core.DataProtector
{
    public interface IDataProtectionRepository
    {
        string Encrypt(string input);

        string Decrypt(string cipherText);
    }
}