﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Models
{
    public class Response
    {
        public bool Status { get; set; }
        public string? Message { get; set; }
    }


    public class TokenResponse
    {
        public bool Status { get; set; }
        public string? Message { get; set; }
        public string? Token { get; set; }
    }


    public class UserData
    {
        public string? Name { get; set; }
        public string? City { get; set; } = string.Empty;
        public string? Contact { get; set; }
        public string? Email { get; set; }
        public string? Roles { get; set; }
        public byte[]? SaltPassword { get; set; }
        public byte[]? HashPassword { get; set; }
    }

}
