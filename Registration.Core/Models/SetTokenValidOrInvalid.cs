﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Core.Models
{
    public class SetTokenValidOrInvalid
    {
        public string? Email { get; set; }
        public string? Token { get; set; }
        public bool SetTokenValid { get; set; }
    }
}
