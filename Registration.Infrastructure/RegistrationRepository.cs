﻿using Microsoft.Extensions.Configuration;
using Registration.Core.Interface;
using Registration.Core.Models;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Registration.Infrastructure
{
    public class RegistrationRepository : IRegistrationRepository
    {
        private readonly IConfiguration _configuration;
        private static string _connectionString = string.Empty;

        public RegistrationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:Registration"]!;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }



        public async Task<Response> SignUp(SignUpParameters signUpParameter)
        {
            CreatePasswordHash(signUpParameter.Password!, out byte[] SaltPassword, out byte[] HashPassword);

            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspSignUp]",
                new
                {
                    signUpParameter.Name,
                    signUpParameter.City,
                    signUpParameter.Contact,
                    signUpParameter.Email,
                    SaltPassword,
                    HashPassword
                }, commandType: CommandType.StoredProcedure);

            return response;
        }

        public async Task<TokenResponse> SignIn(SignInParameters signInParameters)
        {
            TokenResponse response = new TokenResponse(); // Initialize the response object
            using IDbConnection db = Connection;

            var result = await db.QueryMultipleAsync("[uspLogin]", new
            {
                signInParameters.Email
            }, commandType: CommandType.StoredProcedure);

            // Read TokenResponse from the first result set
            response = result.Read<TokenResponse>().FirstOrDefault()!;

            // If response is not successful, return it
            if (!response.Status)
            {
                return response;
            }

            // Read UserData from the second result set
            var receivedUserData = result.Read<UserData>().FirstOrDefault();

            // Verify password and generate token
            if (!VerifyPasswordHash(signInParameters.Password!, receivedUserData?.SaltPassword!, receivedUserData?.HashPassword!))
            {
                response.Message = $"Invalid Password : {signInParameters.Password}";
                return response;
            }

            response.Token = GenerateToken(receivedUserData!);
            return response;
        }


        public async Task<Response> LogOut(SetTokenValidOrInvalid setTokenValidOrInvalid)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspLogout]", setTokenValidOrInvalid,commandType: CommandType.StoredProcedure).ConfigureAwait(false);
            return response;
        }


        public async Task<Response> SetRole(SetRoleParemeter setRoleParemeter)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspSetRole]", setRoleParemeter, commandType: CommandType.StoredProcedure);
            return response;
        }



        public static void CreatePasswordHash(string password, out byte[] SaltPassword, out byte[] HashPassword)
        {
            var mySalt = BCrypt.Net.BCrypt.GenerateSalt();
            SaltPassword = Encoding.ASCII.GetBytes(mySalt);

            HashPassword = Encoding.ASCII.GetBytes(BCrypt.Net.BCrypt.HashPassword(password, mySalt));
        }

        public static bool VerifyPasswordHash(string password, byte[] storedSalt, byte[] storedHash)
        {
            try
            {
                string storedSaltStr = Encoding.ASCII.GetString(storedSalt);
                var newPassword = BCrypt.Net.BCrypt.HashPassword(password, storedSaltStr);
                string oldPassword = Encoding.Default.GetString(storedHash);
                return (newPassword == oldPassword);
            }
            catch
            {
                throw;
            }
        }

        public static string GenerateToken(UserData userData)
        {
            //var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]!));
            //var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"],
            //    (IEnumerable<Claim>)signInParameters, expires: DateTime.Now.AddMinutes(15), signingCredentials: credentials);

            //return new JwtSecurityTokenHandler().WriteToken(token);



            //TokenResponse response = new();
            var tokenHandler = new JwtSecurityTokenHandler();
            const string secretKey = "sklhui@jbpfj)#tbn7_+s8+7!4j1*fmxsq=k-2%-zcvt1#g==9sqs#";
            var key = Encoding.ASCII.GetBytes(secretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name , userData.Name!),
                    new Claim(ClaimTypes.MobilePhone , userData.Contact !),
                    new Claim(ClaimTypes.Locality, userData.City !),
                    new Claim(ClaimTypes.Role,userData.Roles!) ,
                    new Claim(ClaimTypes.Email, userData.Email!)
                }),

                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            //response.Status = true;
            //response.Message = "Login and Token Generated SuccessFull";
            //response.Token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
            //return response;

            return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

        }


        public async Task<Response> CheckTokenValid(UserTokenInfo userTokenInfo)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspCheckTokenValid]", userTokenInfo, commandType: CommandType.StoredProcedure);
            return response;
        }

        public async Task<Response> SetTokenVaild(SetTokenValidOrInvalid setTokenValidOrInvalid)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspSetTokenValidOrInvalid]", setTokenValidOrInvalid, commandType: CommandType.StoredProcedure);
            return response;
        }
    }
}