﻿using Registration.Core.DataProtector;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Registration.INFRASTRUCTURE.InfraHelper
{
    public static class RepoCommon
    {
        private static string environment;

        static RepoCommon()
        {
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "";
        }

        public static string GetAppSettingValue(IDataProtectionRepository _dataProtectionService, string key)
        {
            string appsettingvalue = string.Empty;

            if (environment == "Staging")
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.Staging.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
                //appsettingvalue = _dataProtectionService.Decrypt(appsettingvalue);
            }
            else if (environment == "Production")
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.Production.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
                //appsettingvalue = _dataProtectionService.Decrypt(appsettingvalue);
            }
            if (environment == "Production")
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.Production.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
                //appsettingvalue = _dataProtectionService.Decrypt(appsettingvalue);
            }
            else
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
            }
            return appsettingvalue;
        }

        public static async Task<HttpResponseMessage> CallApi(Uri objUri, string body, HttpMethod httpMethod, List<ApiHeaderExtraHeaders>? apiHeaderExtraHeaders = null)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (apiHeaderExtraHeaders?.Count > 0)
            {
                foreach (var apiHeader in apiHeaderExtraHeaders)
                {
                    if (apiHeader != null)
                    {
                        client.DefaultRequestHeaders.Add(apiHeader.name ?? "", apiHeader.values ?? new List<string>());
                    }
                }
            }
            var request = new HttpRequestMessage
            {
                RequestUri = objUri,
                Method = ((httpMethod != null) ? httpMethod : HttpMethod.Post)
            };

            request.Content = (httpMethod == HttpMethod.Get) ? null : new StringContent((body), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.SendAsync(request).ConfigureAwait(false);
            return response;
        }

        public static async Task<HttpResponseMessage> CallJiraAPI(Uri objUri, object body, HttpMethod httpMethod, IDataProtectionRepository dataProtectionRepository, string contentType, List<ApiHeaderExtraHeaders>? apiHeaderExtraHeaders = null)
        {
            HttpClient client = new HttpClient();
            var jiraEmail = GetAppSettingValue(dataProtectionRepository, "JiraAPISettings:JiraEmail");
            var jiraApiKey = GetAppSettingValue(dataProtectionRepository, "JiraAPISettings:JiraAPIKey");
            var auth = Encoding.ASCII.GetBytes(string.Concat(jiraEmail, ":", jiraApiKey));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            if (apiHeaderExtraHeaders != null && apiHeaderExtraHeaders.Any())
            {
                foreach (var headers in apiHeaderExtraHeaders)
                {
                    client.DefaultRequestHeaders.Add(headers.name ?? "", headers.values ?? new List<string>());
                }
            }

            var request = new HttpRequestMessage()
            {
                RequestUri = objUri,
                Method = ((httpMethod != null) ? httpMethod : HttpMethod.Post),
            };

            request.Content = (httpMethod == HttpMethod.Get) ? null : new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, contentType);
            HttpResponseMessage response = await client.SendAsync(request);
            return response;
        }

        public static async Task<HttpResponseMessage> CallJiraTicketAttachmentAPI(Uri objUri, IFormFile file, HttpMethod httpMethod, IDataProtectionRepository dataProtectionRepository, List<ApiHeaderExtraHeaders>? apiHeaderExtraHeaders = null)
        {
            HttpClient client = new HttpClient();
            var jiraEmail = GetAppSettingValue(dataProtectionRepository, "JiraAPISettings:JiraEmail");
            var jiraApiKey = GetAppSettingValue(dataProtectionRepository, "JiraAPISettings:JiraAPIKey");
            var auth = Encoding.ASCII.GetBytes(string.Concat(jiraEmail, ":", jiraApiKey));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            if (apiHeaderExtraHeaders != null && apiHeaderExtraHeaders.Any())
            {
                foreach (var headers in apiHeaderExtraHeaders)
                {
                    client.DefaultRequestHeaders.Add(headers.name ?? "", headers.values ?? new List<String>());
                }
            }

            var request = new HttpRequestMessage()
            {
                RequestUri = objUri,
                Method = ((httpMethod != null) ? httpMethod : HttpMethod.Post),
            };

            using var fileStream = file.OpenReadStream();
            byte[] bytes = new byte[file.Length];
            fileStream.Read(bytes, 0, (int)file.Length);

            var multipart = new MultipartFormDataContent();
            multipart.Add(new ByteArrayContent(bytes), "file", file.FileName);
            request.Content = multipart;

            HttpResponseMessage response = await client.SendAsync(request);
            return response;
        }

        public static void AddLogTofile(string key, string value)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString() + Environment.NewLine);
            sb.Append(string.Concat(key, ":", value));
            sb.Append(Environment.NewLine + Environment.NewLine);
            File.AppendAllText("ContentFiles/Logfiles/log.txt", sb.ToString());
            sb.Clear();
        }

        public static string GetErrorMessage(string key)
        {
            string appsettingvalue = string.Empty;
            IConfigurationRoot configuration = new ConfigurationBuilder().
            SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
            AddJsonFile($"messages.json").
            Build();
            appsettingvalue = configuration.GetValue<string>(key) ?? "";
            return appsettingvalue;
        }

        public static int GetStatusCodeBykey(string key)
        {
            int statusCode = 0;
            switch (key)
            {
                case "token-not-match":
                    {
                        statusCode = StatusCodes.Status410Gone;
                    }
                    break;
                case "security-response-not-matched":
                    {
                        statusCode = StatusCodes.Status410Gone;
                    }
                    break;
                case "duplicate":
                    {
                        statusCode = StatusCodes.Status409Conflict;
                    }
                    break;
                case "not-found":
                    {
                        statusCode = StatusCodes.Status404NotFound;
                    }
                    break;
                default:
                    {
                        statusCode = StatusCodes.Status500InternalServerError;
                    }
                    break;
                    ;

            }
            return statusCode;
        }
    }

    public class ApiHeaderExtraHeaders
    {
        public string? name { get; set; }
        public List<string>? values { get; set; }
    }


}