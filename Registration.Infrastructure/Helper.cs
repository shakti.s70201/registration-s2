﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registration.Infrastructure
{
    public static class EmailSubjectHelper
    {
        public static string LoginOTPSubject = "New User Login OTP | {{company-name}}";
        public static string UpdatePasswordOTPSubject = "Verify User OTP | {{company-name}}";
        public static string ResetPasswordSubject = "Reset Password request | {{company-name}}";
        public static string AccountLockSubject = "User Account Lock | {{company-name}}";
        public static string UserCreatedSubject = "New User Temporary Password | {{company-name}}";
        

    }

    public static class FlagHelper
    {
        public static string ResetPasswordScreen = "reset-password";
        public static string ChangePasswordScreen = "change-password";


    }


    public static class  MessageHelper
    {
        public static string NoMessage = "Message is not assigned";
    }
}
