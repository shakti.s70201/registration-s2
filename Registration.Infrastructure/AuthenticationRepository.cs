﻿using AuthLibrary;
using Response = CrudOperations.Response;
using Microsoft.Extensions.Configuration;
using Registration.Core.Authentication;
using Registration.Core.DataProtector;
using CrudOperations;
using CommonComponent.Email;
using System.Text;
using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.AspNetCore.Mvc;
using Registration.Core.Common;

namespace Registration.Infrastructure
{
    public class AuthenticationRepository : IAuthenticationRepository
    {

        private readonly IConfiguration _configuration;
        private readonly IAuthLib _authLib;
        private readonly ICrudOperationService _crudOperationService;
        private readonly IEmailService _emailService;
        private readonly IDataProtectionRepository _dataProtectionService;





        public AuthenticationRepository(IConfiguration configuration, IAuthLib authLib, ICrudOperationService crudOperationService, IEmailService emailService, IDataProtectionRepository dataProtectionService)
        {
            _configuration = configuration;
            _authLib = authLib;
            _crudOperationService = crudOperationService;
            _emailService = emailService;
            _dataProtectionService = dataProtectionService;
        }
        public async Task<Response> UserCreation(DtoUserCreationParameters dtoUserCreationParameters)
        {

            DataAccess.CreatePasswordHash(dtoUserCreationParameters.Password, out byte[] passwordHash, out byte[] passwordSalt);

            //dtoUserCreationDetails.Name =  Utilty.ToTitleCase(dtoUserCreationDetails.Name ?? "");

            return await _authLib.SignUp<Response>("[uspUserCreation]", new
            {
                dtoUserCreationParameters.Name,
                dtoUserCreationParameters.Gender,
                dtoUserCreationParameters.Contact,
                dtoUserCreationParameters.Email,
                dtoUserCreationParameters.DOB,
                dtoUserCreationParameters.Role,
                dtoUserCreationParameters.IsTwoFactorAuthenticationRequired,
                dtoUserCreationParameters.CreatedBy,
                passwordHash,
                passwordSalt

            });

        }


        public async Task<AuthResponse<UserInformationDetails>> UserLogin(DtoUserLoginParemeter userLoginParemeter)
        {
            var response = await _authLib.ValidateLoginCredentials<UserInformationDetails>("[uspUserLogin]", new
            {
                userLoginParemeter.Email
            }, userLoginParemeter.Password);

            return response;
        }


        public async Task<Response<DtoInvalidAttemptResponse>> InvalidLoginAttemptChecker(DtoInvalidAttemptChecker attemptChecker)
        {
            var response = await _crudOperationService.GetSingleRecord<DtoInvalidAttemptResponse>("[uspUserInvalidLoginAttemptChecker]", attemptChecker);
            if (!response.Status && response.Message == "ACL")
            {
                //var emailBody = await _emailService.GetEmailContent("AccountLocked", new
                //{

                //});
                //await _emailService.SendEmail(attemptChecker?.Email ?? "", "Account Locked Due to Invalid Login Attempts", emailBody, true);

            }
            return response;
        }

        //------------------  OTP Verification & Generate JWT Token   ------------------
        public async Task<Response<UserInformationDetails>> VerifyOtpAsync(DtoVerifyOtp verifyOtp)
        {
            string? resultMessage;
            Response<UserInformationDetails> userDetailsResponse = new();

            AuthResponse verifyOtpResponse = await _authLib.VerifyOneTimePassword("[uspUserVerifyOTP]", new
            {
                verifyOtp?.UserId,
                verifyOtp?.Otp

            }).ConfigureAwait(false);

            if (verifyOtpResponse.Status)
            {

                UserDetailsGetParameters userDetailsGetParameters = new()
                {
                    UserId = verifyOtp?.UserId
                };
                userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParameters).ConfigureAwait(false);



                if (userDetailsResponse.Status)
                {
                    UserInformationDetails user = userDetailsResponse.Data;

                    var token = await _authLib.GenerateJWTToken(user.UserId?.ToString(), user.Email, user.Role, expiration: DateTime.UtcNow.AddDays(1)).ConfigureAwait(false);

                    //Saving the Login Data
                    await _authLib.SaveToken("[uspUserTokenSave]", new
                    {
                        verifyOtp?.UserId,
                        Token = token,
                        //verifyOtp?.DeviceToken,
                        //verifyOtp?.DevicePlatform,
                        //verifyOtp?.DeviceId

                    }).ConfigureAwait(false);

                    //if (!string.IsNullOrWhiteSpace(verifyOtp?.DeviceToken))
                    //{
                    //    var _ = await _crudOperationService.InsertUpdateDelete<Response>("uspUserTokenAdd", new
                    //    {
                    //        user.UserId,
                    //        Token = verifyOtp.DeviceToken,
                    //        DeviceType = verifyOtp.DevicePlatform,
                    //        verifyOtp.DeviceId,
                    //        CreatedBy = "System"
                    //    }).ConfigureAwait(false);
                    //}

                    userDetailsResponse.Data.Token = token;

                    resultMessage = Utilty.GetDataFromXML("SuccessMessage.xml", "success", "user-success-login");
                    userDetailsResponse.Message = resultMessage;
                }
                else
                {
                    resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", userDetailsResponse.Message);
                    userDetailsResponse.Message = resultMessage;

                }


                //if(!LoginResponse.Status)
                //{
                //    string? errorMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "unf");
                //    LoginResponse.Message = errorMessage;
                //}
            }
            else
            {
                string? errorMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", verifyOtpResponse.Message);
                userDetailsResponse.Status = false;
                userDetailsResponse.Message = errorMessage;

            }

            return userDetailsResponse;
        }

        public async Task<Response<UserInformationDetails>> GetUserBasicDetailsAsync(UserDetailsGetParameters userDetailsGetParameters)
        {
            var response = await _crudOperationService.GetSingleRecord<UserInformationDetails>("[uspUserBasicDeatilsGet]", new
            {
                userDetailsGetParameters.UserId,
                userDetailsGetParameters.Email

            });

            return response;
        }

        public async Task<Response<DtoOtpDetails>> ResendOtpAsync(DtoOtpDetails dtoOtpDetails)
        {

            string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);


            DtoSaveOtpParam SaveOtpParam = new()
            {
                Otp = otp,
                UserId = dtoOtpDetails.UserId
            };
            Response<DtoOtpDetails> otpDetailsResponse = await SaveOtpAsync(SaveOtpParam).ConfigureAwait(false);

            if (otpDetailsResponse.Status)
            {
                UserDetailsGetParameters userDetailsGetParam = new()
                {
                    UserId = dtoOtpDetails.UserId
                };

                Response<UserInformationDetails> userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParam).ConfigureAwait(false);

                //string? companyName = _configuration.GetValue<string>("CompanyName");
                string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                string body;
                using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{{otp}}", otp)
                    .Replace("{{username}}", userDetailsResponse.Data.Name)
                    .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyName"))
                    .Replace("{{adminemail}}", adminEmail);

                string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);
            }
            return otpDetailsResponse;
        }



        //-------------------   Forget Password & Generated Link Token   --------------------------
        public async Task<Response<DtoForgotPassword>> ForgotPasswordVerifyAsync(DtoForgotPasswordRequest forgotPasswordRequest)
        {
            var validEmailResponse = await CheckValidEmailAsync(forgotPasswordRequest).ConfigureAwait(false);
            string UserName = validEmailResponse.Data;

            string? resultMessage;

            Response<DtoForgotPassword> objResponseForgotPassword = new();
            DtoForgotPassword objForgotPassword = new();

            if (validEmailResponse.Status)
            {
                string emailToken = Utilty.RandomString(10);
                //await _authLib.GenerateOTP(6).ConfigureAwait(false);

                DtoPasswordVerifyTokenParam passwordVerifyTokenParam = new()
                {
                    EmailId = forgotPasswordRequest?.Email,
                    Token = emailToken,
                };
                var PasswordValidatorResponse = await SavePasswordVerifyTokenAsync(passwordVerifyTokenParam).ConfigureAwait(false);

                #region Send Mail
                UserDetailsGetParameters userDetailsGetParam = new()
                {
                    Email = forgotPasswordRequest?.Email
                };

                Response<UserInformationDetails> userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParam);

                string? link = "";
                if (userDetailsResponse.Status)
                {

                    var user = userDetailsResponse.Data;

                    link = _configuration?.GetValue<string>("VerifyPasswordUrl")?
                   .Replace("{{EMAIL}}", _dataProtectionService.Encrypt(forgotPasswordRequest!.Email!), StringComparison.InvariantCulture).
                   Replace("{{TOKEN}}", _dataProtectionService.Encrypt(emailToken), StringComparison.InvariantCulture);

                    string? mailSenderName = _configuration?.GetValue<string>("CompanyNameMailSender")!;

                    string body;
                    using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "ForgotPasswordVerifyMail.html")))
                    {
                        body = reader.ReadToEnd();
                    }


                    body = body.Replace("{{username}}", user.Name)
                        .Replace("{{LINK}}", link)
                        .Replace("{{sendername}}", mailSenderName);

                    string mailSubject = EmailSubjectHelper.ResetPasswordSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                    //string Subject = EmailSubjectHelper.ResetPasswordSubject;

                    _ = _emailService?.SendEmail(forgotPasswordRequest?.Email ?? "", mailSubject, body, true);

                }


                #endregion Send Mail



                resultMessage = Utilty.GetDataFromXML("SuccessMessage.xml", "success", "password-reset-mail-success");
                objResponseForgotPassword.Message = resultMessage;
                objResponseForgotPassword.Status = true;

                objForgotPassword.Link = link;
                objForgotPassword.Email = _dataProtectionService.Encrypt(forgotPasswordRequest?.Email ?? "");
                objForgotPassword.EmailToken = _dataProtectionService.Encrypt(emailToken);

                objResponseForgotPassword.Data = objForgotPassword;

            }
            else
            {

                objResponseForgotPassword.Status = false;
                objResponseForgotPassword.Message = validEmailResponse.Message;
            }

            return objResponseForgotPassword;


        }

        public async Task<Response> CheckValidEmailAsync(DtoForgotPasswordRequest? forgotPasswordRequest)
        {
            var validEmailResponse = await _crudOperationService.InsertUpdateDelete<Response>("[uspUserCheckValidEmail]", new { forgotPasswordRequest?.Email }).ConfigureAwait(false);

            return validEmailResponse;
        }

        public async Task<Response> SavePasswordVerifyTokenAsync(DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {


            var tokenSaveResponse = await _crudOperationService.InsertUpdateDelete<CrudOperations.Response>("[uspUserSavePasswordVerifyToken]", new
            {
                passwordVerifyTokenParam.EmailId,
                passwordVerifyTokenParam.Token

            }).ConfigureAwait(false);

            return tokenSaveResponse;


        }


        //----------------------    Verifing Link Token     -----------------------------
        public async Task<Response<DtoUserVerifyDetails>> VerifyPasswordTokenAsync(DtoPasswordVerifyTokenParam passwordVerifyTokenParam)
        {
            DtoUserVerifyDetails objUserVerifyDetails = new();

            passwordVerifyTokenParam.EmailId = _dataProtectionService.Decrypt(passwordVerifyTokenParam.EmailId ?? "");
            passwordVerifyTokenParam.Token = !string.IsNullOrEmpty(passwordVerifyTokenParam?.Token) ? (_dataProtectionService.Decrypt(passwordVerifyTokenParam.Token)) : "";


            Response<DtoUserVerifyDetails> verifyTokenResponse = await _crudOperationService.InsertUpdateDelete<Response<DtoUserVerifyDetails>>("[uspUserVerifyPasswordToken]", new
            {
                passwordVerifyTokenParam?.EmailId,
                passwordVerifyTokenParam?.Token
            }).ConfigureAwait(false);

            if (verifyTokenResponse.Status)
            {
                UserDetailsGetParameters userDetailsGetParameters = new()
                {
                    Email = passwordVerifyTokenParam?.EmailId
                };

                Response<UserInformationDetails> userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParameters).ConfigureAwait(false);

                var userDetails = userDetailsResponse.Data;
                objUserVerifyDetails.UserId = userDetails.UserId;
                objUserVerifyDetails.EmailId = userDetails.Email;


                string otp = await _authLib.GenerateOTP(6).ConfigureAwait(false);

                DtoSaveOtpParam SaveOtpParam = new()
                {
                    Otp = otp,
                    UserId = userDetails.UserId
                };
                Response<DtoOtpDetails> otpDetailsResponse = await SaveOtpAsync(SaveOtpParam).ConfigureAwait(false);

                string? adminEmail = _configuration.GetValue<string>("AdminEmail");

                string body;
                using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{{otp}}", otp)
                    .Replace("{{username}}", userDetailsResponse.Data.Name)
                    .Replace("{{sendername}}", _configuration.GetValue<string>("CompanyName"))
                    .Replace("{{adminemail}}", adminEmail);

                string mailSubject = EmailSubjectHelper.UpdatePasswordOTPSubject.Replace("{{company-name}}", _configuration.GetValue<string>("CompanyName"));

                _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);


                if (otpDetailsResponse.Status)
                {
                    verifyTokenResponse.Message = "OTP sent to your registered email id";
                    verifyTokenResponse.Data = objUserVerifyDetails;
                }
            }
            return verifyTokenResponse;
        }

        public async Task<Response<DtoOtpDetails>> SaveOtpAsync(DtoSaveOtpParam saveOtpParam)
        {

            Response<DtoOtpDetails> otpDetailsResponse = new();
            string? resultMessage = "";

            var otpSaveResponse = await _authLib.SaveToken("[uspUserSaveOtp]",
                new
                {
                    saveOtpParam.Otp,
                    saveOtpParam.UserId
                }).ConfigureAwait(false);


            if (otpSaveResponse.Status)
            {
                UserDetailsGetParameters userDetailsGetParameters = new() { UserId = saveOtpParam.UserId };

                Response<UserInformationDetails> userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParameters);

                if (userDetailsResponse.Status)
                {
                    UserInformationDetails user = (UserInformationDetails)userDetailsResponse.Data;


                    string body;
                    using (StreamReader reader = new(Path.Combine("ContentFiles", "EmailTemplate", "LoginOtpMail.html")))
                    {
                        body = reader.ReadToEnd();
                    }

                    string? companyName = _configuration.GetValue<string>("CompanyNameMailSender");
                    string? adminEmail = _configuration.GetValue<string>("AdminEmail");
                    string? mailSenderName = _configuration?.GetValue<string>("CompanyNameMailSender")!;

                    body = body
                        .Replace("{{username}}", user.Name)
                        .Replace("{{otp}}", saveOtpParam.Otp)
                        .Replace("{{sendername}}", mailSenderName)
                        .Replace("{{adminemail}}", adminEmail);

                    string Subject = EmailSubjectHelper.LoginOTPSubject;

                    _emailService?.SendEmail(userDetailsResponse?.Data.Email ?? "", Subject, body, true);

                    DtoOtpDetails otpDetails = new()
                    {
                        AdminEmail = adminEmail,
                        UserId = user.UserId,
                        IsTwoFactorAuthenticationRequired = user.IsTwoFactorAuthenticationRequired
                    };

                    resultMessage = Utilty.GetDataFromXML("SuccessMessage.xml", "success", "otp-sent-success");

                    otpDetailsResponse.Status = true;
                    otpDetailsResponse.Data = otpDetails;
                    otpDetailsResponse.Message = resultMessage;
                }
                else
                {
                    resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", userDetailsResponse.Message);
                    otpDetailsResponse.Status = false;
                    otpDetailsResponse.Message = resultMessage;
                }




            }
            else
            {
                resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "login-otp-not-generated");
                otpDetailsResponse.Message = resultMessage;
            }

            return otpDetailsResponse;

        }


        //----------------------    Reset password async    ----------------------------
        public async Task<Response> ResetPasswordAsync(DtoResetPasswordParam resetPasswordParam)
        {
            Response? response = new();

            if (resetPasswordParam != null)
            {
                string email = resetPasswordParam!.IsEncrypted! ? _dataProtectionService.Decrypt(resetPasswordParam.Email ?? "") : resetPasswordParam.Email!;
                string emailToken = !string.IsNullOrEmpty(resetPasswordParam?.EmailToken) ?
                (resetPasswordParam.IsEncrypted ? _dataProtectionService.Decrypt(resetPasswordParam.EmailToken) : resetPasswordParam.EmailToken) : "";

                resetPasswordParam!.Email = (email ?? "");
                resetPasswordParam!.EmailToken = emailToken;

                resetPasswordParam!.Opr = _dataProtectionService.Encrypt(resetPasswordParam.Password!);

                DataAccess.CreatePasswordHash(resetPasswordParam.Password, out byte[] passwordHash, out byte[] passwordSalt);
                Response ValidateOldPasswordResponse = await ValidateNewPassWithOldPassAsync(resetPasswordParam.Password!, null, resetPasswordParam.Email).ConfigureAwait(false);

                if (ValidateOldPasswordResponse.Status)
                {
                    string resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "password-already-exists")!;
                    response.Status = false;
                    response.Message = resultMessage.Replace("{prev-password-limit}", ValidateOldPasswordResponse.Data);
                    response.Data = "duplicate";
                    return response;

                }
                else
                {
                    resetPasswordParam.PasswordHash = passwordHash;
                    resetPasswordParam.PasswordSalt = passwordSalt;


                    response = await _crudOperationService.InsertUpdateDelete<Response>("[uspUserResetPassword]", new
                    {
                        resetPasswordParam.Email,
                        resetPasswordParam.PasswordHash,
                        resetPasswordParam.PasswordSalt,
                        resetPasswordParam.UpdatedBy
                    }).ConfigureAwait(false);


                    #region Mail Send

                    UserDetailsGetParameters userDetailsGetParam = new() { Email = resetPasswordParam.Email };

                    Response<UserInformationDetails> userDetailsResponse = await GetUserBasicDetailsAsync(userDetailsGetParam);

                    if (userDetailsResponse.Status)
                    {
                        UserInformationDetails user = (UserInformationDetails)userDetailsResponse.Data;
                        string body = "", Subject = "";

                        string adminEmail = _configuration.GetValue<string>("AdminEmail");

                        string? mailSenderName = _configuration?.GetValue<string>("CompanyNameMailSender")!;


                        if (resetPasswordParam.ScreenName == FlagHelper.ResetPasswordScreen)
                        {
                            using (StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "ResetPasswordUpdated.html")))
                            {
                                body = reader.ReadToEnd();
                            }

                            body = body
                            .Replace("{{username}}", user.Name)
                            .Replace("{{useraccountname}}", user.Email)
                            .Replace("{{sendername}}", mailSenderName)
                            .Replace("{{adminemail}}", adminEmail);


                            Subject = EmailSubjectHelper.LoginOTPSubject;


                        }
                        else if (resetPasswordParam.ScreenName == FlagHelper.ChangePasswordScreen)
                        {
                            using (StreamReader reader = new StreamReader(Path.Combine("ContentFiles", "EmailTemplate", "PasswordChange.html")))
                            {
                                body = reader.ReadToEnd();
                            }

                            body = body
                            .Replace("{{username}}", user.Name)
                            .Replace("{{sendername}}", mailSenderName)
                            .Replace("{{adminemail}}", adminEmail);


                            Subject = EmailSubjectHelper.LoginOTPSubject;
                        }
                        _emailService?.SendEmail(userDetailsResponse?.Data.Email ?? "", Subject, body, true);

                    }

                    #endregion End Mail Send
                }
            }

            return response;
        }

        public async Task<Response> ValidateNewPassWithOldPassAsync(string password, string? userId, string? email)
        {
            ResponseList<DtoUserOldPasswords> response;

            response = await _crudOperationService.GetList<DtoUserOldPasswords>("[uspUserGetOldPasswordDetails]", new
            {
                userId,
                email

            });

            if (response.Status)
            {

                List<DtoUserOldPasswords> userOldPasswordList = response.Data;

                if (VerifyOldPasswordHash(password, userOldPasswordList))
                {
                    return new Response
                    {
                        Status = true,
                        Message = "passwordavailableinlist",
                        Data = response.Message
                    };
                }
                else
                {
                    return new Response
                    {
                        Status = false,
                        Message = "passwordnotavailableinlist",
                        Data = response.Message
                    };
                }
                //response.Data = oldPasswordList;
            }
            else
            {
                return new Response
                {
                    Status = false,
                    Message = "Password does not exists.",
                    Data = response.Message
                };
            }


        }

        public bool VerifyOldPasswordHash(string password, List<DtoUserOldPasswords> userOldPasswords)
        {
            for (int counter = 0; counter < userOldPasswords.Count; counter++)
            {
                byte[]? storedSalt = userOldPasswords[counter].PasswordSalt;
                byte[]? storedHash = userOldPasswords[counter].PasswordHash;

                var computedHash = BCryptNet.HashPassword(password, Encoding.ASCII.GetString((storedSalt!)));

                var storedHashSting = Encoding.ASCII.GetString(storedHash!);

                if (computedHash.Equals(storedHashSting))
                {
                    return true;
                }

            }
            return false;
        }


        //----------------------    Change Password    ---------------------------------
        public async Task<Response> ChangePasswordAsync(DtoChangePasswordRequestParam changePasswordRequestParam)
        {


            #region Verify Current Password

            DtoUserLoginParemeter dtoLoginParemeter = new()
            {
                Email = changePasswordRequestParam.Email,
                Password = changePasswordRequestParam.OldPassword
            };

            var loginResponse = await _authLib.ValidateLoginCredentials<UserInformationDetails>("[uspUserLogin]", new
            {

                dtoLoginParemeter.Email,

            }, dtoLoginParemeter.Password).ConfigureAwait(false);


            if (!loginResponse.Status)
            {
                Response oldPasswordVerifyResponse = new()
                {
                    Status = false,
                    Data = "not-found",
                    Message = "Old Password is not valid"
                };
                return oldPasswordVerifyResponse;

            }

            #endregion Verify Current Password



            DtoResetPasswordParam resetPasswordParam = new()
            {
                Email = changePasswordRequestParam.Email,
                EmailToken = changePasswordRequestParam.EmailToken,
                Password = changePasswordRequestParam.Password,
                IsEncrypted = false,
                ScreenName = changePasswordRequestParam.ScreenName

            };

            var changePasswordResponse = await ResetPasswordAsync(resetPasswordParam).ConfigureAwait(false);

            return changePasswordResponse;

        }


        //------------------------------------------------------------------------------
        public async Task<ResponseList<DtoMasterDropdownList>> GetDropDownMasterAsync(DtoDropDownTypeReq param)
        {
            return await _crudOperationService.GetList<DtoMasterDropdownList>(storedProcedureName: "[uspGetAllMasterDropDown]", param);
        }
    }


}

