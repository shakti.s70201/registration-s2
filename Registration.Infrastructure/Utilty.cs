﻿using log4net;
using log4net.Config;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Globalization;
using System.Reflection;
using System.Xml.Linq;


//using Microsoft.Extensions.Configuration.FileExtensions;

namespace Registration.Infrastructure
{
    public class Utilty
    {
        static string? environment;
        private static readonly Random random = new();

        public Utilty()
        {
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "";
        }

        public static string ToTitleCase(string value)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);
        }


        public static string GetAppSettingValue(string key)
        {
            string appsettingvalue;
            if (environment == "Staging")
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.Staging.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
                //appsettingvalue = _dataProtectionService.Decrypt(appsettingvalue);
            }
            else
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().
                SetBasePath(AppDomain.CurrentDomain.BaseDirectory).
                AddJsonFile($"appsettings.json").
                Build();
                appsettingvalue = configuration.GetValue<string>(key) ?? "";
            }
            return appsettingvalue;
        }

        public static string? GetDataFromXML(string fileName, string xmlHeaderTag, string key)
        {
            
            string cmlErrorMessageFilePath = Path.Combine("ContentFiles", "TextHelper", fileName);

            XDocument xdoc = XDocument.Load(cmlErrorMessageFilePath);
            
            string? value = (from node in xdoc?.Descendants(xmlHeaderTag)
                             select (string?)node?.Element(key))?
                            .First();

            string? message = string.IsNullOrWhiteSpace(value) ? MessageHelper.NoMessage : value;

            return message;
            
            

        }

        public static void LogDetails<T>(string message)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            ILog _logger = LogManager.GetLogger(typeof(T));
            _logger.Info(message);

            //const string connectionString = "mongodb://localhost:27017";

            //// Create a MongoClient object by using the connection string
            //var client = new MongoClient(connectionString);

            ////Use the MongoClient to access the server
            //var database = client.GetDatabase("BoilerPlate");

            ////get mongodb collection
            //var collection = database.GetCollection<Logger>("logger");
            //await collection.InsertOneAsync(new Logger { Message = Message });


        }

        public static IDictionary<string, object> GetValues(object obj)
        {
            return obj.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(obj))!;
        }


        public static byte[]? GenerateExcelFileByData(List<dynamic> Data, string fileName)
        {
            FileInfo newFile = new(fileName);
            string flag = "";


            if (Data.Count > 0)
            {
                using (ExcelPackage package = new(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileName);
                    //ExcelWorksheet worksheet2 = package.Workbook.Worksheets.Add("UserDetails");

                    #region add Worksheet1 data
                    foreach (var row in Data)
                    {
                        var d = Utilty.GetValues(row);
                        int colHeaderCounter1 = 0;
                        foreach (KeyValuePair<string, object> header in d)
                        {
                            colHeaderCounter1++;

                            worksheet.Cells[1, colHeaderCounter1].Value = header.Key.Replace("_", " ");

                            worksheet.Cells[1, colHeaderCounter1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[1, colHeaderCounter1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSteelBlue);
                            //System.Drawing.Color.LightSteelBlue
                        }
                        //break;
                    }


                    int x = 2;
                    foreach (var row in Data)
                    {
                        var d1 = Utilty.GetValues(row);
                        int y = 0;
                        foreach (KeyValuePair<string, object> data in d1)
                        {
                            y += 1;
                            if (data.Value is bool)
                            {
                                flag = (bool)data.Value ? "YES" : "NO";
                                worksheet.Cells[x, y].Value = flag;
                            }
                            else
                            {
                                worksheet.Cells[x, y].Value = data.Value;
                            }
                        }
                        x++;
                    }

                    //for (int m = 1; m <= worksheet.Dimension.End.Column; m++)
                    //{
                    //    worksheet.Column(m).AutoFit();
                    //}



                    worksheet.Cells["A1:XFD1"].Style.Font.Bold = true;
                    worksheet.View.PageLayoutView = false;


                    #endregion

                    return package.GetAsByteArray();
                    
                }
            }
            return null;
        }

        
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }
}
