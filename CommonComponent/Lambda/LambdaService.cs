﻿using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using Newtonsoft.Json;
using System.Text;

namespace CommonComponent.Lambda
{
    public class LambdaService : ILambdaService
    {
        private readonly string _scheduleFunctionName;
        private readonly string _sendEmailFunctionName;
        private readonly string _sendNotificationFunctionName;
        private readonly string _sendReminderFunctionName;
        private readonly AmazonLambdaClient _client;

        public LambdaService(string accessKey,
            string secretKey,
            string regionEndpoint,
            string scheduleFunctionName,
            string sendEmailFunctionName,
            string sendNotificationFunctionName,
            string sendReminderFunctionName)
        {
            _scheduleFunctionName = scheduleFunctionName;
            _sendEmailFunctionName = sendEmailFunctionName;
            _sendNotificationFunctionName = sendNotificationFunctionName;
            _client = new(accessKey, secretKey, RegionEndpoint.GetBySystemName(regionEndpoint));
            _sendReminderFunctionName = sendReminderFunctionName;
        }

        public async Task<string> InvokeSchedulerAsync(LambdaRequestPayload lambdaRequest)
        {
            return await InvokeAsync(_scheduleFunctionName, lambdaRequest);
        }

        public async Task CancelInvoke(string arn)
        {
            await InvokeAsync(_scheduleFunctionName, new LambdaRequestPayload
            {
                ARNId = arn,
            });
        }

        public async Task InvokeEmailAsync(EmailRequest request)
        {
            await InvokeAsync(_sendEmailFunctionName, request);
        }

        public async Task InvokeNotificationAsync(NotificationRequest request)
        {
            await InvokeAsync(_sendNotificationFunctionName, request);
        }

        public async Task InvokeSendReminder(LambdaRequestPayload request)
        {
            await InvokeAsync(_sendReminderFunctionName, request);
        }

        private async Task<string> InvokeAsync(string functionName, object payload)
        {
            try
            {
                InvokeRequest ir = new()
                {
                    FunctionName = functionName,
                    InvocationType = InvocationType.RequestResponse,
                    Payload = JsonConvert.SerializeObject(payload)
                };

                InvokeResponse response = await _client.InvokeAsync(ir);
                string respStr = Encoding.ASCII.GetString(response.Payload.ToArray());
                var result = JsonConvert.DeserializeObject<AwsLamdaResponse>(respStr);

                return result?.ARNId ?? string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}