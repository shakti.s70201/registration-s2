﻿using Newtonsoft.Json;

namespace CommonComponent.Lambda
{
    public class AwsLamdaResponse
    {
        [JsonProperty("executionArn")]
        public string? ARNId { get; set; }

        [JsonProperty("startDate")]
        public DateTime? StartDate { get; set; }
    }
}