﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;

namespace CommonComponent.DynamoDB
{
    public class DynamoDBService : IDynamoDBService
    {
        private readonly AmazonDynamoDBClient _dynamoDBclient;
        public DynamoDBService(string accessKey, string secretKey, string region)
        {
            _dynamoDBclient = new AmazonDynamoDBClient(accessKey, secretKey, Amazon.RegionEndpoint.GetBySystemName(region));
        }

        public async Task<IEnumerable<string>> Scan(string tableName)
        {
            var table = Table.LoadTable(_dynamoDBclient, tableName);

            var config = new ScanOperationConfig
            {
                Select = SelectValues.AllAttributes
            };

            var search = table.Scan(config);

            List<Document> documentList = new();

            do
            {
                documentList.AddRange(await search.GetNextSetAsync());
            } while (!search.IsDone);

            return documentList.Select(x => x.ToJsonPretty());
        }

        public async Task Insert(string tableName, string data)
        {
            var document = Document.FromJson(data);

            var table = Table.LoadTable(_dynamoDBclient, tableName);

            await table.PutItemAsync(document).ConfigureAwait(false);
        }
    }
}
