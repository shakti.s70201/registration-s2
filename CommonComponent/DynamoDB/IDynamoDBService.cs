﻿namespace CommonComponent.DynamoDB
{
    public interface IDynamoDBService
    {
        Task<IEnumerable<string>> Scan(string tableName);

        Task Insert(string tableName, string data);
    }
}
