﻿using Microsoft.AspNetCore.Http;

namespace CommonComponent.Storage
{
    public interface IStorageService
    {
        Task<string> Download(string filePath, int ExpireIn = 60);

        Task<string> Upload(IFormFile formFile, string fileName, string filePath);

        Task<string> Upload(byte[] file, string fileName, string filePath);

        Task UploadByPath(string sourceFilePath, string fileName, string filePath);

        Task Delete(string filePath);
    }
}