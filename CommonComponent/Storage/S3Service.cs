﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;

namespace CommonComponent.Storage
{
    public class S3Service : IStorageService
    {
        private readonly string _rootBucketName;
        private readonly string _accesskey;
        private readonly string _secretkey;
        private readonly string _regionEndpoint;
        private readonly AmazonS3Client _client;

        public S3Service(string rootBucketName, string accessKey, string secretKey, string regionEndpoint)
        {
            _rootBucketName = rootBucketName;
            _accesskey = accessKey;
            _secretkey = secretKey;
            _regionEndpoint = regionEndpoint;

            _client = new AmazonS3Client(_accesskey, _secretkey, Amazon.RegionEndpoint.GetBySystemName(_regionEndpoint));
        }

        public async Task<string> Download(string filePath, int ExpireIn)
        {
            return await Task.Run(() =>
            {
                try
                {
                    string DocUrl = _client.GetPreSignedURL(new GetPreSignedUrlRequest
                    {
                        BucketName = _rootBucketName,
                        Key = filePath,
                        Expires = DateTime.UtcNow.AddMinutes(ExpireIn)
                    });

                    return DocUrl;
                }
                catch (Exception)
                {
                    throw;
                }
            });
        }

        public async Task<string> Upload(IFormFile formFile, string fileName, string filePath)
        {
            try
            {
                var fileTransferUtility = new TransferUtility(_client);

                using var memoryStream = new MemoryStream();

                await formFile.CopyToAsync(memoryStream);
                await fileTransferUtility.UploadAsync(memoryStream, _rootBucketName + "/" + filePath, fileName);
                fileTransferUtility.Dispose();

                return filePath + "/" + fileName;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<string> Upload(byte[] file, string fileName, string filePath)
        {
            try
            {
                using var fileTransferUtility = new TransferUtility(_client);

                using var ms = new MemoryStream(file);

                await fileTransferUtility.UploadAsync(ms, _rootBucketName + "/" + filePath, fileName);

                return filePath + "/" + fileName;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task UploadByPath(string filePath, string fileName, string rootBucketPath)
        {
            try
            {
                var fileTransferUtility = new TransferUtility(_client);

                byte[] file = File.ReadAllBytes(filePath);
                Stream stream = new MemoryStream(file);
                await fileTransferUtility.UploadAsync(stream, _rootBucketName + "/" + rootBucketPath, fileName);

                fileTransferUtility.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool ValidateExtensions(List<string> lstExtensions, string currentExtension)
        {
            return lstExtensions.Contains(currentExtension.ToUpper());
        }

        public async Task Delete(string filePath)
        {
            try
            {
                await _client.DeleteObjectAsync(new DeleteObjectRequest() { BucketName = _rootBucketName, Key = filePath });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}