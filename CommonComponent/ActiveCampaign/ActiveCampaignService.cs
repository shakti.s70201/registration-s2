﻿using Newtonsoft.Json;
using RestSharp;

namespace CommonComponent.ActiveCampaign
{
    public class ActiveCampaignService : IActiveCampaignService
    {
        private readonly string _accountName;
        private readonly string _apiKey;
        private readonly RestClient _restClient;

        public ActiveCampaignService(string accountName, string apiKey)
        {
            _accountName = accountName;
            _apiKey = apiKey;

            _restClient = new RestClient();
        }

        public async Task SyncContactAsync(ContactSyncBody body)
        {
            try
            {
                var request = ContactSyncRequest.From(body);
                if (body.Fields != null)
                {
                    // Get All fields from API
                    var fields = await GetAllFields();
                    if (fields.Succeeded && fields.Data != null)
                    {
                        // Map Fields with data provided
                        var mappedValues = await MapFieldIdAndValues(body.Fields, fields.Data);

                        request.Contact!.FieldValues = mappedValues.Succeeded && mappedValues.Data != null ? mappedValues.Data : default;
                    }
                }

                var httpRequest = new RestRequest(string.Format(ActiveCampaignConstant.SyncContactUrl, _accountName), Method.Post);

                httpRequest.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

                httpRequest.AddBody(request, ActiveCampaignConstant.JsonContentType);

                var result = await _restClient.ExecuteAsync(httpRequest);
                if (!result.IsSuccessful)
                {
                    throw new Exception(result.Content);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task AddTagToContact(string email, params string[] tags)
        {
            try
            {
                var contactRes = await GetContact(email);
                if (contactRes.Succeeded)
                {
                    var tagsRes = await GetAllTags();
                    if (tagsRes.Succeeded)
                    {
                        foreach (var item in tags)
                        {
                            var tag = tagsRes.Data?.FirstOrDefault(x => x.Tag?.Trim().ToLower() == item.Trim().ToLower());
                            if (tag == null)
                            {
                                var addTagRes = await CreateTag(item);
                                if (addTagRes.Succeeded)
                                {
                                    tag = addTagRes.Data;
                                }
                            }

                            var res = await AddContactTag(contactRes.Data?.Contacts?.First().Id ?? string.Empty, tag?.Id ?? string.Empty);
                            if (!res.Succeeded)
                                throw new Exception(res.Message);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task RemoveContactTag(string email, string tag)
        {
            try
            {
                var contactRes = await GetContact(email);
                if (contactRes.Succeeded)
                {
                    var contact = contactRes.Data?.Contacts?.First();
                    var tagObj = contactRes.Data?.Tags?.FirstOrDefault(x => x.Tag?.Trim().ToLower() == tag.Trim().ToLower());
                    if (tagObj != null)
                    {
                        var removeRes = await RemoveContactTag(contactRes.Data?.ContactTags?.FirstOrDefault(x => x.Tag == tagObj.Id && x.Contact == contact?.Id)?.Id ?? string.Empty);
                        if (!removeRes.Succeeded)
                            throw new Exception(removeRes.Message);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal async Task<Response<ContactGet>> GetContact(string email)
        {
            var response = new Response<ContactGet>();

            var request = new RestRequest(string.Format(ActiveCampaignConstant.GetContactUrl, _accountName, email), Method.Get);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var result = await _restClient.ExecuteAsync(request);

            if (result.IsSuccessful && result.Content != null)
                response.SetSuccess(JsonConvert.DeserializeObject<ContactGet>(result.Content));
            else
                response.SetError(result?.Content);

            return response;
        }

        internal async Task<Response> AddContactTag(string contactId, string tagId)
        {
            var response = new Response();

            var request = new RestRequest(string.Format(ActiveCampaignConstant.AddTagToContactUrl, _accountName), Method.Post);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var data = new
            {
                contactTag = new
                {
                    contact = contactId,
                    tag = tagId
                }
            };

            request.AddBody(data, ActiveCampaignConstant.JsonContentType);

            var result = await _restClient.ExecuteAsync(request);
            response.Succeeded = true;
            if (!result.IsSuccessful)
                response.SetError(result?.Content);

            return response;
        }

        internal async Task<Response> RemoveContactTag(string contactTagId)
        {
            var response = new Response();
            var request = new RestRequest(string.Format(ActiveCampaignConstant.DeleteContactTagUrl, _accountName, contactTagId), Method.Delete);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var result = await _restClient.ExecuteAsync(request);
            response.Succeeded = true;
            if (!result.IsSuccessful)
                response.SetError(result?.Content);

            return response;
        }

        internal async Task<Response<List<Field>>> GetAllFields()
        {
            Response<List<Field>> response = new();
            var request = new RestRequest(string.Format(ActiveCampaignConstant.GetAllFieldsUrl, _accountName), Method.Get);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var result = await _restClient.ExecuteAsync(request);

            if (result.IsSuccessful && result.Content != null)
                response.SetSuccess(JsonConvert.DeserializeObject<FieldDeserializeObj>(result.Content).Fields);
            else
                response.SetError(result?.Content);

            return response;
        }

        internal async Task<Response<List<Tags>>> GetAllTags()
        {
            Response<List<Tags>> response = new();

            var request = new RestRequest(string.Format(ActiveCampaignConstant.GetAllTagsUrl, _accountName), Method.Get);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var result = await _restClient.ExecuteAsync(request);

            if (result.IsSuccessful && result.Content != null)
                response.SetSuccess(JsonConvert.DeserializeObject<TagsDeserialize>(result.Content).Tags);
            else
                response.SetError(result?.Content);

            return response;
        }

        internal async Task<Response<List<FieldValue>>> MapFieldIdAndValues(object fieldValueObj, List<Field> fieldList)
        {
            Response<List<FieldValue>> response = new();
            var data = new List<FieldValue>();

            foreach (var item in fieldValueObj.GetType().GetProperties())
            {
                // Check the property name with field title from active campaign
                var field = fieldList.FirstOrDefault(x => x.Title.ToUpper() == item.Name.ToUpper());

                if (field == null)
                {
                    var fieldAddRes = await CreateField(item.Name);
                    if (fieldAddRes.Succeeded)
                    {
                        field = fieldAddRes.Data;
                    }
                    else
                    {
                        response.SetError(fieldAddRes.Message);
                        continue;
                    }
                }

                data.Add(new FieldValue
                {
                    Field = field!.Id,
                    Value = item?.GetValue(fieldValueObj)?.ToString() ?? string.Empty
                });
            }

            response.SetSuccess(data);
            return response;
        }

        internal async Task<Response<Tags>> CreateTag(string tag)
        {
            Response<Tags> response = new();
            var request = new RestRequest(string.Format(ActiveCampaignConstant.CreateTagUrl, _accountName), Method.Post);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var data = new
            {
                tag = new
                {
                    tag = tag.Trim(),
                    tagType = "contact",
                    description = tag
                }
            };
            request.AddBody(data, ActiveCampaignConstant.JsonContentType);

            var result = await _restClient.ExecuteAsync(request);

            if (result.IsSuccessful && result.Content != null)
            {
                response.SetSuccess(JsonConvert.DeserializeObject<SingleTagDeserialize>(result.Content).Tag);
            }
            else
                response.SetError(result?.Content);
            return response;
        }

        internal async Task<Response<Field>> CreateField(string title)
        {
            Response<Field> response = new();
            var request = new RestRequest(string.Format(ActiveCampaignConstant.CreateFieldUrl, _accountName), Method.Post);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var data = new
            {
                field = new
                {
                    title = title.Trim(),
                    type = "text",
                    perstag = title.Replace(" ", "_").ToUpper(),
                    show_in_list = true,
                    visible = true
                }
            };
            request.AddBody(data, ActiveCampaignConstant.JsonContentType);
            var result = await _restClient.ExecuteAsync(request);
            response.Succeeded = true;
            if (result.IsSuccessful && result.Content != null)
            {
                response.SetSuccess(JsonConvert.DeserializeObject<SingleFieldDeserializeObj>(result.Content).Field);
                var relRes = await CreateFieldRel(response.Data!.Id!);
                if (!relRes.Succeeded)
                {
                    response.SetError(relRes.Message);
                }
            }
            else
                response.SetError(result?.Content);
            return response;
        }

        internal async Task<Response> CreateFieldRel(string id)
        {
            Response response = new();
            var request = new RestRequest(string.Format(ActiveCampaignConstant.CreateFieldRelUrl, _accountName), Method.Post);
            request.AddHeader(ActiveCampaignConstant.AuthHeaderKey, _apiKey);

            var data = new
            {
                fieldRel = new
                {
                    field = id,
                    relid = 0
                }
            };

            request.AddBody(data, ActiveCampaignConstant.JsonContentType);

            var result = await _restClient.ExecuteAsync(request);
            response.Succeeded = true;
            if (!result.IsSuccessful)
                response.SetError(result?.Content);
            return response;
        }
    }
}