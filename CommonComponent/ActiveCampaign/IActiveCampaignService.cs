﻿namespace CommonComponent.ActiveCampaign
{
    public interface IActiveCampaignService
    {
        /// <summary>
        /// Sync contact details on active campaign
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        Task SyncContactAsync(ContactSyncBody body);

        /// <summary>
        /// Add a tag or list of tags to a contact
        /// </summary>
        /// <param name="email"> Email of the contact </param>
        /// <param name="tags"> List of tags or single tag to add to a contact </param>
        /// <returns></returns>
        Task AddTagToContact(string email, params string[] tags);

        /// <summary>
        /// Remove a tag from contact
        /// </summary>
        /// <param name="email"> Email of the contact </param>
        /// <param name="tag"> Tag to be removed </param>
        /// <returns></returns>
        Task RemoveContactTag(string email, string tag);
    }
}