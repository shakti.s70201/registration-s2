﻿namespace CommonComponent.ActiveCampaign
{
    public class ContactBase
    {
        public string? Email { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Phone { get; set; }
        public string? Id { get; set; }
    }

    public class ContactSyncBody : ContactBase
    {
        public object? Fields { get; set; }
    }

    internal class ContactSyncRequest
    {
        public Contact? Contact { get; set; }

        internal static ContactSyncRequest From(ContactSyncBody body)
        {
            return new ContactSyncRequest
            {
                Contact = new Contact
                {
                    Email = body.Email,
                    FirstName = body.FirstName,
                    LastName = body.LastName,
                    Phone = body.Phone
                }
            };
        }
    }

    internal class Contact : ContactBase
    {
        public List<FieldValue>? FieldValues { get; set; }
    }

    internal class ContactGet
    {
        public List<Tags>? Tags { get; set; }

        public List<ContactTag>? ContactTags { get; set; }

        public List<ContactBase>? Contacts { get; set; }
    }

    internal class TagsDeserialize
    {
        public List<Tags>? Tags { get; set; }
    }

    internal class SingleTagDeserialize
    {
        public Tags? Tag { get; set; }
    }

    internal class Tags
    {
        public string? Tag { get; set; }

        public string? Id { get; set; }
    }

    internal class ContactTag
    {
        public string? Contact { get; set; }
        public string? Tag { get; set; }

        public string? Id { get; set; }
    }

    internal class FieldValue
    {
        public string? Field { get; set; }
        public string? Value { get; set; }
    }

    internal class FieldDeserializeObj
    {
        public List<Field>? Fields { get; set; }
    }

    internal class SingleFieldDeserializeObj
    {
        public Field? Field { get; set; }
    }

    internal class Field
    {
        public string? Id { get; set; }
        public string? Type { get; set; }
        public string? Perstag { get; set; }
        public string? Title { get; set; }
    }

    internal static class ActiveCampaignConstant
    {
        public const string GetAllFieldsUrl = "https://{0}/api/3/fields";
        public const string CreateFieldUrl = "https://{0}/api/3/fields";
        public const string CreateFieldRelUrl = "https://{0}/api/3/fieldRels";
        public const string SyncContactUrl = "https://{0}/api/3/contact/sync";
        public const string GetContactUrl = "https://{0}/api/3/contacts?filters[email]={1}&include=contactTags.tag";
        public const string DeleteContactTagUrl = "https://{0}/api/3/contactTags/{1}";
        public const string AddTagToContactUrl = "https://{0}/api/3/contactTags";
        public const string GetAllTagsUrl = "https://{0}/api/3/tags";
        public const string CreateTagUrl = "https://{0}/api/3/tags";

        public const string AuthHeaderKey = "Api-Token";
        public const string JsonContentType = "application/json";
    }

    public class Response
    {
        public bool Succeeded { get; set; }

        public string? Message { get; set; }

        public void SetError(string? message)
        {
            Succeeded = false;
            Message = message;
        }

        public void SetSuccess()
        {
            Succeeded = true;
        }
    }

    public class Response<T>
    {
        public bool Succeeded { get; set; }

        public string? Message { get; set; }

        public T? Data { get; set; }

        public void SetError(string? message)
        {
            Succeeded = false;
            Message = message;
        }

        public void SetSuccess(T? data)
        {
            Succeeded = true;
            Data = data;
        }
    }
}