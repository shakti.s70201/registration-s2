﻿using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace CommonComponent.Message
{
    public class SMSService : ISMSService
    {
        private static MessageConfig? _messageConfig;

        public SMSService(MessageConfig messageConfig)
        {
            _messageConfig = messageConfig;
        }

        public async Task<CommonComponentResponse> SendSMS(string recipient, string body)
        {
            CommonComponentResponse response = new();
            try
            {
                if (_messageConfig == null)
                {
                    response.Message = "Message config is not provided.";
                    return response;
                }

                TwilioClient.Init(_messageConfig.AccountSID, _messageConfig.TwilioAuthToken);

                var message = await MessageResource.CreateAsync(
                     body: body,
                     from: new PhoneNumber(_messageConfig.FromNumber),
                     to: new PhoneNumber(recipient.Replace("-", ""))
                 );
                // If message status is not any of the failed status, we return success
                response.Status = !new List<MessageResource.StatusEnum> { MessageResource.StatusEnum.Failed, MessageResource.StatusEnum.Undelivered, MessageResource.StatusEnum.Canceled }.Contains(message.Status);
            }
            catch (ApiException e)
            {
                response.Status = false;
                if (e.Code == 21614)
                {
                    response.Message = "Uh oh, looks like this caller can't receive SMS messages.";
                }
                response.Message = e.Message;
            }
            return response;
        }

        public async Task<CommonComponentResponse> SendSMS(List<string> recipient, string body)
        {
            CommonComponentResponse response = new();

            foreach (var item in recipient)
            {
                response = await SendSMS(item, body); // TODO: Handle response
            }

            return response;
        }
    }
}