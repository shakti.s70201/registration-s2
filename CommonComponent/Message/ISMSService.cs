﻿namespace CommonComponent.Message
{
    public interface ISMSService
    {
        Task<CommonComponentResponse> SendSMS(string recipient, string body);

        Task<CommonComponentResponse> SendSMS(List<string> recipient, string body);
    }
}