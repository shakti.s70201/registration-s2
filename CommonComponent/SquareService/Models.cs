﻿using Square.Models;

namespace CommonComponent.SquareService
{
    public class CustomerResult
    {
        public string? Id { get; set; }
        public string? FamilyName { get; set; }
        public string? Nickname { get; set; }
        public string? CompanyName { get; set; }
        public string? EmailAddress { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Birthday { get; set; }
        public string? ReferenceId { get; set; }
        public string? Note { get; set; }

        internal static CustomerResult From(Customer customer)
        {
            return new CustomerResult
            {
                Id = customer.Id,
                FamilyName = customer.FamilyName,
                Nickname = customer.Nickname,
                CompanyName = customer.CompanyName,
                EmailAddress = customer.EmailAddress,
                PhoneNumber = customer.PhoneNumber,
                Birthday = customer.Birthday,
                ReferenceId = customer.ReferenceId,
                Note = customer.Note
            };
        }
    }

    public class CardResult
    {
        public string? Id { get; set; }

        public string? CardBrand { get; set; }

        public string? Last4 { get; set; }

        public long? ExpMonth { get; set; }

        public long? ExpYear { get; set; }

        public string? CardholderName { get; set; }

        public string? Fingerprint { get; set; }

        public string? CustomerId { get; set; }

        public string? MerchantId { get; set; }

        public string? ReferenceId { get; set; }

        public bool? Enabled { get; set; }

        public string? CardType { get; set; }

        public string? PrepaidType { get; set; }

        public string? Bin { get; set; }

        public long? Version { get; set; }

        public string? CardCoBrand { get; set; }

        internal static CardResult From(Card card)
        {
            return new CardResult
            {
                Id = card.Id,
                CardBrand = card.CardBrand,
                Last4 = card.Last4,
                ExpMonth = card.ExpMonth,
                ExpYear = card.ExpYear,
                Fingerprint = card.Fingerprint,
                CustomerId = card.CustomerId,
                MerchantId = card.MerchantId,
                ReferenceId = card.ReferenceId,
                Enabled = card.Enabled,
                CardType = card.CardType,
                PrepaidType = card.PrepaidType,
                Bin = card.Bin,
                Version = card.Version,
                CardCoBrand = card.CardCoBrand
            };
        }
    }

    public class PaymentLinkResult
    {
        public string? Id { get; set; }

        public string? OrderId { get; set; }

        public string? Url { get; set; }
        public string? PaymentNote { get; set; }

        internal static PaymentLinkResult From(PaymentLink paymentLink)
        {
            return new PaymentLinkResult
            {
                Id = paymentLink.Id,
                OrderId = paymentLink.OrderId,
                Url = paymentLink.Url,
                PaymentNote = paymentLink.PaymentNote
            };
        }
    }

    public class PaymentRefundResult
    {
        public string? Id { get; set; }

        public decimal Amount { get; set; }

        public string? Status { get; set; }

        public string? PaymentId { get; set; }

        public string? OrderId { get; set; }

        public string? Reason { get; set; }

        internal static PaymentRefundResult From(PaymentRefund paymentRefund)
        {
            return new PaymentRefundResult
            {
                Id = paymentRefund.Id,
                Amount = paymentRefund.AmountMoney.Amount / 100m ?? 0,
                Status = paymentRefund.Status,
                PaymentId = paymentRefund.PaymentId,
                OrderId = paymentRefund.OrderId,
                Reason = paymentRefund.Reason
            };
        }
    }

    public class PayoutResult
    {
        public string? Id { get; set; }

        public string? Status { get; set; }

        public string? CreatedAt { get; set; }

        public IEnumerable<PayoutEntryObject>? PayoutEntries { get; set; }
    }

    public class PayoutEntryObject
    {
        public decimal GrossAmountMoney { get; set; }

        public decimal FeeAmountMoney { get; set; }

        public decimal NetAmountMoney { get; set; }

        public string? EffectiveAt { get; set; }

        public string? PaymentId { get; set; }

        public string? CustomerId { get; set; }
    }

    public class PaymentDetail
    {
        public string PaymentId { get; set; } = "";

        public string CustomerId { get; set; } = "";
    }

    public class SubscriptionPlan
    {
        public string Id { get; set; } = "";

        public string PlanId { get; set; } = "";

        public string Status { get; set; } = "";

        public string StartDate { get; set; } = "";

        public string EndDate { get; set; } = "";

        public string ChargedThroughDate { get; set; } = "";


        public static SubscriptionPlan From(Subscription subscription)
        {
            return new SubscriptionPlan
            {
                Id = subscription.Id,
                PlanId = subscription.PlanId,
                Status = subscription.Status,
                StartDate = subscription.CreatedAt,
                ChargedThroughDate = subscription.ChargedThroughDate,
                EndDate = subscription.CanceledDate
            };
        }
    }

    public class OrderInfo
    {
        public string OrderId { get; set; } = "";

        public bool IsSubscriptionPayment { get; set; }
        public string Source { get; set; } = "";
        public string CustomerId { get; set; } = "";
        public static OrderInfo From(Order order)
        {
            return new OrderInfo
            {
                OrderId = order.Id,
                IsSubscriptionPayment = order.Source.Name.ToLower().Contains("subscription"),
                Source = order.Source.Name,
                CustomerId = order.CustomerId
            };
        }
    }

    public class LineItems
    {
        public string Id { get; set; } = "";
    }
}