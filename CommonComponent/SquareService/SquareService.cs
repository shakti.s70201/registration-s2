﻿using Square;
using Square.Exceptions;
using Square.Models;

namespace CommonComponent.SquareService
{
    public class SquareService : ISquareService
    {
        private readonly SquareClient _client;
        private readonly string _locationId;

        public SquareService(string accessToken, string locationId)
        {
            _client = new SquareClient.Builder()
                .Environment(Square.Environment.Sandbox)
                .AccessToken(accessToken)
                .Build();
            _locationId = locationId;
        }

        #region Subscription

        public async Task<string> UpsertSubscriptionPlan(string subscriptionId, string subscriptionName, SubscriptionPlanDuration duration, long amount)
        {
            var recurringPriceMoney = new Money.Builder()
              .Amount(amount)
              .Currency("USD")
              .Build();

            var subscriptionPhase = new SubscriptionPhase.Builder(cadence: MapPlanDuration(duration))
              .RecurringPriceMoney(recurringPriceMoney)
              .Build();

            var phases = new List<SubscriptionPhase>
            {
                subscriptionPhase
            };

            var subscriptionPlanData = new CatalogSubscriptionPlan.Builder(name: subscriptionName, phases)
              .Build();

            var subscriptionObject = new CatalogObject.Builder(type: "SUBSCRIPTION_PLAN",
                id: string.IsNullOrWhiteSpace(subscriptionId) ? "#" + Guid.NewGuid().ToString() : subscriptionId)
              .SubscriptionPlanData(subscriptionPlanData)
              .Build();

            var body = new UpsertCatalogObjectRequest.Builder(idempotencyKey: Guid.NewGuid().ToString(), mObject: subscriptionObject)
              .Build();

            try
            {
                var result = await _client.CatalogApi.UpsertCatalogObjectAsync(body: body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                return result.CatalogObject.Id;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> DeleteSubscriptionPlan(string planId)
        {
            try
            {
                var result = await _client.CatalogApi.DeleteCatalogObjectAsync(planId);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                return true;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<string> SubscribeToPlan(string customerId, string planId, string cardId)
        {
            var body = new CreateSubscriptionRequest.Builder(
                locationId: _locationId,
                planId: planId,
                customerId: customerId)
              .CardId(cardId)
              .Source(new SubscriptionSource(planId + " - Subscription"))
              .Build();

            try
            {
                var result = await _client.SubscriptionsApi.CreateSubscriptionAsync(body: body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                return result.Subscription.Id;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> CancelSubscription(string subscriptionId, string reason)
        {
            var body = new PauseSubscriptionRequest(pauseReason: reason);

            try
            {
                var result = await _client.SubscriptionsApi.PauseSubscriptionAsync(subscriptionId, body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                return true;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> ResumeSubscription(string subscriptionId)
        {
            var body = new ResumeSubscriptionRequest.Builder()
                .ResumeChangeTiming("IMMEDIATE")
                .Build();

            try
            {
                var result = await _client.SubscriptionsApi.ResumeSubscriptionAsync(subscriptionId: subscriptionId, body: body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                return true;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> UpdateSubscriptionCardId(string subscriptionId, string cardId)
        {
            var subscription = new Subscription.Builder()
                .CardId(cardId)
                .Build();

            var body = new UpdateSubscriptionRequest.Builder()
                .Subscription(subscription)
                .Build();

            try
            {
                var result = await _client.SubscriptionsApi.UpdateSubscriptionAsync(subscriptionId: subscriptionId, body: body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }

                return true;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> ScheduleChangeSubscriptionPlan(string subscriptionId, string newPlanId)
        {
            var body = new SwapPlanRequest.Builder(newPlanId: newPlanId)
                .Build();

            try
            {
                var result = await _client.SubscriptionsApi.SwapPlanAsync(subscriptionId: subscriptionId, body: body);
                if (result.Errors != null)
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
                else
                {
                    return true;
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<string> ChangeSubscriptionPlan(string customerId, string cardId, string subscriptionId, string newPlanId)
        {
            try
            {
                var res = await _client.SubscriptionsApi.CancelSubscriptionAsync(subscriptionId);
                if (res.Errors.Count == 0)
                {
                    return await SubscribeToPlan(customerId, newPlanId, cardId);
                }
                else
                {
                    throw new Exception(res.Errors.First()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<SubscriptionPlan> GetSubscription(string customerId)
        {
            var customerIds = new List<string>
            {
                customerId
            };

            var filter = new SearchSubscriptionsFilter.Builder()
              .CustomerIds(customerIds)
              .Build();

            var query = new SearchSubscriptionsQuery.Builder()
              .Filter(filter)
              .Build();

            var body = new SearchSubscriptionsRequest.Builder()
              .Limit(1)
              .Query(query)
              .Build();

            try
            {
                var result = await _client.SubscriptionsApi.SearchSubscriptionsAsync(body: body);
                if (result.Errors.Count < 1)
                {
                    if (!result.Subscriptions.Any(x => x.Status == "ACTIVE"))
                    {
                        throw new Exception("Customer not subscribed");
                    }
                    else
                    {
                        var response = await _client.SubscriptionsApi.RetrieveSubscriptionAsync(result.Subscriptions.First()?.Id);
                        if (response.Errors.Count < 1)
                        {
                            return SubscriptionPlan.From(response.Subscription);
                        }
                        else
                        {
                            throw new Exception(response.Errors.First()?.Detail);
                        }
                    }
                }
                else
                {
                    throw new Exception(result.Errors.First()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        #endregion Subscription

        public async Task<OrderInfo> GetOrderDetails(string orderId)
        {
            try
            {
                var response = await _client.OrdersApi.RetrieveOrderAsync(orderId);
                if (response.Errors == null)
                {

                    return OrderInfo.From(response.Order);
                }
                else
                {
                    throw new Exception(response.Errors.First()?.Detail);
                }

            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<CustomerResult> CreateCustomer(string email)
        {
            var request = new CreateCustomerRequest.Builder().EmailAddress(email).Build();
            try
            {
                var existingCustomer = await CheckCustomerExistance(email);
                if (existingCustomer == null)
                {
                    var response = await _client.CustomersApi.CreateCustomerAsync(request);
                    if (response.Errors == null)
                    {
                        return CustomerResult.From(response.Customer);
                    }
                    else
                    {
                        throw new Exception(response.Errors.First()?.Detail);
                    }
                }
                else
                {
                    return CustomerResult.From(existingCustomer);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<CardResult> CreateCard(string customerId, string cardToken, string cardHolderName)
        {
            var card = new Card.Builder().CardholderName(cardHolderName).CustomerId(customerId).Build();
            var request = new CreateCardRequest.Builder(Guid.NewGuid().ToString(), cardToken, card).Build();
            try
            {
                var response = await _client.CardsApi.CreateCardAsync(request);
                if (response.Errors == null)
                {
                    return CardResult.From(response.Card);
                }
                else
                {
                    throw new Exception(response.Errors.First()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<bool> CreatePayment(string customerId, string sourceId, long money)
        {
            var moneyObj = new Money.Builder().Amount(money).Currency("USD").Build();
            var request = new CreatePaymentRequest.Builder(sourceId, Guid.NewGuid().ToString(), moneyObj).CustomerId(customerId).Note(customerId).Build();

            try
            {
                var response = await _client.PaymentsApi.CreatePaymentAsync(request);
                if (response.Errors == null)
                {
                    return response.Payment.Status == "COMPLETED"; // "PENDING", "APPROVED"
                }
                else
                {
                    throw new Exception(response.Errors.First()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<IEnumerable<CardResult>> GetCards(string squareCustomerId)
        {
            try
            {
                var response = await _client.CardsApi.ListCardsAsync(customerId: squareCustomerId);
                if (response.Errors == null)
                {
                    return response!.Cards?.Select(CardResult.From)!;
                }
                else
                {
                    throw new Exception(response.Errors.FirstOrDefault()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }
        public async Task<bool> DeleteCard(string cardId)
        {
            try
            {
                var response = await _client.CardsApi.DisableCardAsync(cardId);
                if (response.Errors == null)
                {
                    // return response.Cards.Select(CardResult.From);
                    // return response.True;
                    return true;
                }
                else
                {
                    throw new Exception(response.Errors.FirstOrDefault()?.Detail);
                }
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<PaymentLinkResult> CreatePaymentLink(string customerId, string email, string phone, long money, string paymentName, string note)
        {
            var metadata = new Dictionary<string, string>()
            {
                { "CustomerId", customerId },
                { "note", note }
            };

            var basePriceMoney = new Money.Builder()
              .Amount(money)
              .Currency("USD")
              .Build();

            var orderLineItem = new OrderLineItem.Builder(quantity: "1")
              .Name(paymentName)
              //.ItemType("CUSTOM_AMOUNT")
              .BasePriceMoney(basePriceMoney)
              .Build();

            var lineItems = new List<OrderLineItem>
            {
                orderLineItem
            };

            var pricingOptions = new OrderPricingOptions.Builder()
              .Build();

            var order = new Order.Builder(locationId: _locationId)
              .CustomerId(customerId)
              .LineItems(lineItems)
              .PricingOptions(pricingOptions)
              .Metadata(metadata)
              .Build();

            var preDataBuilder = new PrePopulatedData.Builder()
                .BuyerEmail(email);

            //if (!string.IsNullOrEmpty(phone))
            //{
            //    phone = phone.Replace("-", ""); // Replace special character
            //    preDataBuilder.BuyerPhoneNumber("+1" + phone); // Append US postal code for mobile number
            //}

            var preData = preDataBuilder.Build();

            var acceptedPaymentMethods = new AcceptedPaymentMethods.Builder()
                .ApplePay(false)
                .GooglePay(false)
                .CashAppPay(false)
                .AfterpayClearpay(false)
                .Build();

            var checkoutOptions = new CheckoutOptions.Builder()
              .AcceptedPaymentMethods(acceptedPaymentMethods)
              .Build();

            var body = new CreatePaymentLinkRequest.Builder()
              .IdempotencyKey(Guid.NewGuid().ToString())
              .Order(order)
              .PrePopulatedData(preData)
              .CheckoutOptions(checkoutOptions)
              .PaymentNote(note)
              .Build();

            try
            {
                var result = await _client.CheckoutApi.CreatePaymentLinkAsync(body: body);
                return PaymentLinkResult.From(result.PaymentLink);
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<PaymentRefundResult> CreateRefund(long amount, string paymentId, string reason)
        {
            var amountMoney = new Money.Builder()
              .Amount(amount)
              .Currency("USD")
              .Build();

            var body = new RefundPaymentRequest.Builder(idempotencyKey: Guid.NewGuid().ToString(), amountMoney: amountMoney)
                .PaymentId(paymentId)
                .Reason(reason)
                .Build();

            try
            {
                var result = await _client.RefundsApi.RefundPaymentAsync(body: body);
                return PaymentRefundResult.From(result.Refund);
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        public async Task<IEnumerable<PayoutResult>> GetPayouts(DateTime from, DateTime to)
        {
            try
            {
                IEnumerable<PayoutResult> payoutEntries = new List<PayoutResult>();
                var payouts = await GetPayoutResponse(from, to);
                foreach (var item in payouts)
                {
                    var result = await GetPayoutEntry(item.Id);
                    payoutEntries = payoutEntries.Append(new PayoutResult
                    {
                        Id = item.Id,
                        Status = item.Status,
                        CreatedAt = item.CreatedAt,
                        PayoutEntries = result
                    });
                }

                return payoutEntries;
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.Errors.FirstOrDefault()?.Detail ?? ex.Message);
            }
        }

        private async Task<IEnumerable<Payout>> GetPayoutResponse(DateTime from, DateTime to, string? cursor = null)
        {
            IEnumerable<Payout> payouts;
            var response = await _client.PayoutsApi.ListPayoutsAsync(beginTime: from.ToString("yyyy-MM-ddTHH:mm:ssZ"), endTime: to.ToString("yyyy-MM-ddTHH:mm:ssZ"), cursor: cursor);
            if (response.Errors == null)
            {
                payouts = response.Payouts;
                if (!string.IsNullOrWhiteSpace(response.Cursor))
                {
                    payouts = payouts.Concat(await GetPayoutResponse(from, to, response.Cursor));
                }
                return payouts;
            }
            else
            {
                throw new Exception(response.Errors.First()?.Detail);
            }
        }

        private async Task<List<PayoutEntryObject>> GetPayoutEntry(string payoutId, string? cursor = null)
        {
            List<PayoutEntryObject> payoutEntries = new();
            var response = await _client.PayoutsApi.ListPayoutEntriesAsync(payoutId, cursor: cursor);
            if (response.Errors == null)
            {
                foreach (var item in response.PayoutEntries)
                {
                    var payment = !string.IsNullOrWhiteSpace(item.TypeChargeDetails.PaymentId) ? await GetPaymentInfo(item.TypeChargeDetails.PaymentId) : null;
                    if (!string.IsNullOrWhiteSpace(payment?.CustomerId))
                        payoutEntries.Add(new PayoutEntryObject
                        {
                            FeeAmountMoney = (item.FeeAmountMoney.Amount / 100m) ?? 0,
                            GrossAmountMoney = (item.GrossAmountMoney.Amount / 100m) ?? 0,
                            NetAmountMoney = (item.NetAmountMoney.Amount / 100m) ?? 0,
                            EffectiveAt = item.EffectiveAt,
                            PaymentId = item.TypeChargeDetails.PaymentId,
                            CustomerId = payment?.CustomerId ?? ""
                        });
                }
                if (!string.IsNullOrWhiteSpace(response.Cursor))
                {
                    payoutEntries.AddRange(await GetPayoutEntry(payoutId, response.Cursor));
                }
                return payoutEntries;
            }
            else
            {
                throw new Exception(response.Errors.First()?.Detail);
            }
        }
        private async Task<PaymentDetail> GetPaymentInfo(string paymentId)
        {
            try
            {
                var response = await _client.PaymentsApi.GetPaymentAsync(paymentId);
                if (response.Errors == null)
                {
                    return new PaymentDetail
                    {
                        PaymentId = response.Payment.Id,
                        CustomerId = response.Payment.CustomerId ?? response.Payment.Note
                    };
                }
                else
                {
                    throw new Exception(response.Errors.First()?.Detail);
                }
            }
            catch (ApiException ex)
            {

                throw new Exception(ex.Errors?.FirstOrDefault()?.Detail ?? ex.Message);
            }

        }

        private async Task<Customer?> CheckCustomerExistance(string customerEmail)
        {
            var filter = new CustomerTextFilter.Builder().Exact(customerEmail.ToLower()).Build();
            var customerFilter = new CustomerFilter.Builder().EmailAddress(filter).Build();
            var query = new CustomerQuery.Builder().Filter(customerFilter).Build();
            var request = new SearchCustomersRequest.Builder().Query(query).Build();

            var response = await _client.CustomersApi.SearchCustomersAsync(request);
            return response.Customers?.FirstOrDefault();
        }

        internal static string MapPlanDuration(SubscriptionPlanDuration duration)
        {
            return duration switch
            {
                SubscriptionPlanDuration.Month => "MONTHLY",
                SubscriptionPlanDuration.Year => "ANNUAL",
                _ => ""
            };
        }
    }

    public enum SubscriptionPlanDuration
    {
        Month = 1,
        Year = 2
    }

}