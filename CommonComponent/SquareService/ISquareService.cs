﻿namespace CommonComponent.SquareService
{
    public interface ISquareService
    {
        /// <summary>
        /// Cancels (Pauses) a subscription
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        Task<bool> CancelSubscription(string subscriptionId, string reason = "");
        
        /// <summary>
        /// Change subscription Plan of a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cardId"></param>
        /// <param name="subscriptionId"></param>
        /// <param name="newPlanId"></param>
        /// <returns></returns>
        Task<string> ChangeSubscriptionPlan(string customerId, string cardId, string subscriptionId, string newPlanId);
        
        /// <summary>
        /// Create a card on file for a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cardToken"></param>
        /// <param name="cardHolderName"></param>
        /// <returns></returns>
        Task<CardResult> CreateCard(string customerId, string cardToken, string cardHolderName);
        
        /// <summary>
        /// Create square customer
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<CustomerResult> CreateCustomer(string email);
        
        /// <summary>
        /// Create a payment to charge customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="sourceId"></param>
        /// <param name="money"></param>
        /// <returns></returns>
        Task<bool> CreatePayment(string customerId, string sourceId, long money);
        
        /// <summary>
        /// Create payment link for some amount to mail patient
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="money"></param>
        /// <param name="paymentName"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        Task<PaymentLinkResult> CreatePaymentLink(string customerId, string email, string phone, long money, string paymentName, string note);
        
        /// <summary>
        /// Initiate a refund process of any amount
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="paymentId"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        Task<PaymentRefundResult> CreateRefund(long amount, string paymentId, string reason);
        
        /// <summary>
        /// Delete customers card on file
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        Task<bool> DeleteCard(string cardId);

        /// <summary>
        /// Get active cards of a customer
        /// </summary>
        /// <param name="squareCustomerId"></param>
        /// <returns></returns>
        Task<IEnumerable<CardResult>> GetCards(string squareCustomerId);

        /// <summary>
        /// Get payouts received from square
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        Task<IEnumerable<PayoutResult>> GetPayouts(DateTime from, DateTime to);

        /// <summary>
        /// Get subscription details for a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<SubscriptionPlan> GetSubscription(string customerId);

        /// <summary>
        /// Resume a paused subscription
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        Task<bool> ResumeSubscription(string subscriptionId);

        /// <summary>
        /// Schedule a cancel of current subscription plan
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="newPlanId"></param>
        /// <returns></returns>
        Task<bool> ScheduleChangeSubscriptionPlan(string subscriptionId, string newPlanId);

        /// <summary>
        /// Subscribe to a new plan for a customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="planId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        Task<string> SubscribeToPlan(string customerId, string planId, string cardId);

        /// <summary>
        /// Update card Id on file for patient subscription
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        Task<bool> UpdateSubscriptionCardId(string subscriptionId, string cardId);

        /// <summary>
        /// Upsert subscription plan details
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="subscriptionName"></param>
        /// <param name="duration"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task<string> UpsertSubscriptionPlan(string subscriptionId, string subscriptionName, SubscriptionPlanDuration duration, long amount);

        /// <summary>
        /// Delete a subscription Plan
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<bool> DeleteSubscriptionPlan(string planId);

        Task<OrderInfo> GetOrderDetails(string orderId);
    }
}