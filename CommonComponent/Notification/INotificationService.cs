﻿namespace CommonComponent.Notification
{
    public interface INotificationService
    {
        Task SendPushNotification(NotificationsDto request);
    }
}