﻿using FirebaseAdmin.Messaging;

namespace CommonComponent.Notification
{
    public class FCMService : INotificationService
    {
        public async Task SendPushNotification(NotificationsDto request)
        {
            var message = new MulticastMessage()
            {
                Notification = new FirebaseAdmin.Messaging.Notification()
                {
                    Title = request.Title,
                    Body = request.Body,
                },
                Tokens = request.Users
            };

            if (request.Data != null)
                message.Data = request.Data;

            await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
        }
    }

    public class NotificationsDto
    {
        public string? Title { get; set; }
        public string? Body { get; set; }
        public List<string>? Users { get; set; }
        public Dictionary<string, string>? Data { get; set; }
    }
}