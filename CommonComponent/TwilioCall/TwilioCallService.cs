﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Video.V1;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace CommonComponent.TwilioCall
{
    public class TwilioCallService : ITwilioCallService
    {
        private readonly string _twilioAccountSid;
        private readonly string _appSid;
        private readonly string _twilioApiKey;
        private readonly string _twilioApiSecret;
        private readonly string _phoneNumber;
        private readonly string _authToken;

        public TwilioCallService(string twilioAccountSid, string appSid, string twilioApiKey, string twilioApiSecret, string phoneNumber, string authToken)
        {
            _twilioAccountSid = twilioAccountSid;
            _appSid = appSid;
            _twilioApiKey = twilioApiKey;
            _twilioApiSecret = twilioApiSecret;
            _phoneNumber = phoneNumber;
            _authToken = authToken;
        }

        public async Task<VoiceResponse> StartCall(string toNumber)
        {
            return await System.Threading.Tasks.Task.Run(() =>
            {
                var voiceResponse = new VoiceResponse();
                var dial = new Dial(callerId: _phoneNumber, timeout: 60);
                //dial.Number(toNumber, statusCallback: new Uri("http://localhost:5002/api/audiocall/statuscallback"),
                //statusCallbackMethod: Twilio.Http.HttpMethod.Post);
                dial.Number(toNumber);
                voiceResponse.Append(dial);
                return voiceResponse;
            });   
        }

        public async Task<Token> TwilioToken(string appointmentType, string givenName)
        {
            return await System.Threading.Tasks.Task.Run(() =>
            {
                string role = string.Empty;

                var grant = new VideoGrant
                {
                    Room = givenName
                };
                var grants = new HashSet<IGrant> { grant };

                var accessToken = new Token(_twilioAccountSid, _twilioApiKey, _twilioApiSecret, role, grants: grants);
                return accessToken;
            });
        }

        public async Task<RoomResource> CreateRoom(string givenName)
        {
            TwilioClient.Init(_twilioAccountSid, _authToken);
            var room = await RoomResource.CreateAsync(type: RoomResource.RoomTypeEnum.PeerToPeer, uniqueName: givenName);
            return room;
        }

        public async Task<RoomResource> EndRoom(string givenName)
        {
            TwilioClient.Init(_twilioAccountSid, _authToken);
            var room =await RoomResource.UpdateAsync(
                   status: RoomResource.RoomStatusEnum.Completed,
                   pathSid: givenName
               );
            return room;
        }
    }
}
