﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio.TwiML;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Video.V1;

namespace CommonComponent.TwilioCall
{
    public interface ITwilioCallService
    {
        Task<VoiceResponse> StartCall(string toNumber);

        Task<Token> TwilioToken(string appointmentType,string givenName);

        Task<RoomResource> CreateRoom(string givenName);
        Task<RoomResource> EndRoom(string givenName);
       
    }
}
