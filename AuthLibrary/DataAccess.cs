﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BCryptNet = BCrypt.Net.BCrypt;

namespace AuthLibrary
{
    public class DataAccess : IAuthLib
    {
        private readonly IConfiguration _configuration;
        private static string _connectionString;

        public DataAccess(IConfiguration configuration, string connectionString)
        {
            _configuration = configuration;
            _connectionString = connectionString;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        public static void SetupDb(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IConfigurationSection GetConfigurationSection(string key)
        {
            return _configuration.GetSection(key);
        }

        public string AppSettingsKeys(string nodeName, string key)
        {
            return _configuration["" + nodeName + ":" + key + ""];
        }

        #region Sign up

        public async Task<T> SignUp<T>(string storedProcedureName, object spParamsPocoMapper)
        {
            T response;

            using (var connections = Connection)
            {
                try
                {
                    response = await connections.QueryFirstOrDefaultAsync<T>(
                        sql: storedProcedureName,
                        param: GenricsDynamicParamterMapper(spParamsPocoMapper),
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
                return response;
            }
        }

        #endregion Sign up

        #region Login

        public async Task<AuthResponse<T>> ValidateLoginCredentials<T>(string spName, object spParametersClassObject, string password)
        {
            using var connections = Connection;
            try
            {
                AuthResponse<T> response;
                using (var result = await connections.QueryMultipleAsync(
                    sql: spName,
                    param: GenricsDynamicParamterMapper(spParametersClassObject),
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure))
                {
                    response = result.Read<AuthResponse<T>>().FirstOrDefault();

                    if (!response.Status)
                    {
                        response.Data = default;
                        return response;
                    }
                    response.Data = result.Read<T>().FirstOrDefault();
                }

                if (!VerifyPasswordHash(password, (byte[])typeof(T).GetProperty("PasswordHash").GetValue(response.Data, null), (byte[])typeof(T).GetProperty("PasswordSalt").GetValue(response.Data, null)))
                {
                    response.Status = false;
                    response.Message = "Invalid password";
                    response.Data = default;
                    return response;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public async Task<AuthResponse<Tuple<T, TRole>>> Login<T, TRole>(string spName, object spParametersClassObject, string password, bool isTwoFactorAuth)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: spName,
                    param: GenricsDynamicParamterMapper(spParametersClassObject),
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure);

                AuthResponse<Tuple<T, TRole>> response = result.Read<AuthResponse<Tuple<T, TRole>>>().FirstOrDefault();

                if (!response.Status)
                {
                    response.Data = default;
                    return response;
                }
                var userData = result.Read<T>().FirstOrDefault();
                var roleData = result.Read<TRole>().FirstOrDefault();
                response.Data = new Tuple<T, TRole>(userData, roleData);
                if (!VerifyPasswordHash(password, (byte[])typeof(T).GetProperty("PasswordHash").GetValue(response.Data.Item1, null), (byte[])typeof(T).GetProperty("PasswordSalt").GetValue(response.Data.Item1, null)))
                {
                    response.Status = false;
                    response.Message = "Invalid password";
                    return response;
                }

                if (!isTwoFactorAuth)
                {
                    var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(_configuration["SecretKey"]);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                          new Claim(ClaimTypes.Name, Convert.ToString(typeof(T).GetProperty("UserId").GetValue(response.Data.Item1, null))),
                          new Claim(ClaimTypes.Role, Convert.ToString(typeof(T).GetProperty("Role").GetValue(response.Data.Item1, null))),
                          new Claim(ClaimTypes.Email, Convert.ToString(typeof(T).GetProperty("Email").GetValue(response.Data.Item1, null)))
                        }
                        ),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var tokenString = tokenHandler.WriteToken(token);

                    typeof(T).GetProperty("Token").SetValue(response.Data.Item1, tokenString);
                }
                else
                {
                    response.Message = "Send OTP using two factor auth";
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        //public async Task<Response> Login<T, TOtpModel>(string spName, object spParametersClassObject, string password, bool isTwoFactorAuth,
        //    string otpSpName = null, object otpSpParametersClassObject = null, string otp = null,
        //    SendOTPOptions sendOTPOption = SendOTPOptions.None, EmailConfig emailConfig = null, MessageConfig messageConfig = null)
        //{
        //    var response = await Login<T>(spName, spParametersClassObject, password, isTwoFactorAuth);
        //    if (!isTwoFactorAuth)
        //    {
        //        return response;
        //    }

        //    using var connections = Connection;
        //    try
        //    {
        //        if (!string.IsNullOrWhiteSpace(otp) && sendOTPOption != SendOTPOptions.None)
        //        {
        //            switch (sendOTPOption)
        //            {
        //                case SendOTPOptions.Email:
        //                    await SendEmailOTP(otp, (string)typeof(T).GetProperty("Email").GetValue(response.Data), emailConfig);
        //                    break;
        //                case SendOTPOptions.SMS:
        //                    await SendSMSOTP(otp, (string)typeof(T).GetProperty("Phone").GetValue(response.Data), messageConfig);
        //                    break;
        //                case SendOTPOptions.All:
        //                    await SendEmailOTP(otp, (string)typeof(T).GetProperty("Email").GetValue(response.Data), emailConfig);
        //                    //await SendSMSOTP(otp, (string)typeof(T).GetProperty("Phone").GetValue(response.Data), messageConfig);
        //                    break;
        //                default: break;
        //            }

        //            typeof(TOtpModel).GetProperty("UserId").SetValue(otpSpParametersClassObject, (string)typeof(T).GetProperty("UserId").GetValue(response.Data));
        //            typeof(TOtpModel).GetProperty("Otp").SetValue(otpSpParametersClassObject, otp);

        //            var result = await connections.QuerySingleAsync<Response>(
        //                sql: otpSpName,
        //                param: GenricsDynamicParamterMapper(otpSpParametersClassObject),
        //                commandTimeout: null,
        //                commandType: CommandType.StoredProcedure);

        //            if (!result.Status)
        //            {
        //                response.Status = false;
        //                response.Message = result.Message;
        //            }
        //        }
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (connections.State == ConnectionState.Open)
        //            connections.Close();
        //    }
        //}

        public async Task<AuthResponse> SaveToken(string spName, object spParametersObject)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QuerySingleAsync<AuthResponse>(
                        sql: spName,
                        param: GenricsDynamicParamterMapper(spParametersObject),
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public async Task<AuthResponse> VerifyOneTimePassword(string spName, object spParametersClassObject)
        {
            using var connections = Connection;
            try
            {
                AuthResponse response = new();

                using (var result = await connections.QueryMultipleAsync(spName, GenricsDynamicParamterMapper(spParametersClassObject), commandType: CommandType.StoredProcedure))
                {
                    response = result.Read<AuthResponse>().FirstOrDefault();
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            var newPassword = BCryptNet.HashPassword(password, Encoding.ASCII.GetString(storedSalt));
            return (newPassword == Encoding.Default.GetString(storedHash));
        }

        public async Task<AuthResponse> Logout(string token, string spName, object spParametersPocoMapper)
        {
            AuthResponse response = new();
            using var connections = Connection;
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    response.Message = "Empty token";
                    return response;
                }

                var handler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken;
                try
                {
                    jwtToken = handler.ReadToken(token.Split(" ")[1]) as JwtSecurityToken;
                }
                catch (Exception)
                {
                    response.Message = "Invalid token";
                    return response;
                }

                if (jwtToken?.Claims?.FirstOrDefault() == null || string.IsNullOrWhiteSpace(jwtToken.Claims.FirstOrDefault().Value))
                {
                    response.Message = "Invalid token";
                    return response;
                }

                response = await connections.QueryFirstAsync<AuthResponse>(
                     sql: spName,
                     param: GenricsDynamicParamterMapper(spParametersPocoMapper),
                     commandTimeout: null,
                     commandType: CommandType.StoredProcedure);

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State != ConnectionState.Closed)
                    connections.Close();
            }
        }

        #endregion Login

        #region Change Password

        public async Task<AuthResponse<T>> ChangePassword<T>(string spName, object spParametersClassObject, string logInSpName, object logInSpParametersClassObject, string currentPassword, string newPassword)
        {
            using var connections = Connection;
            var response = new AuthResponse<T>();
            try
            {
                var logInRes = await ValidateLoginCredentials<T>(logInSpName, logInSpParametersClassObject, currentPassword);
                if (!logInRes.Status)
                {
                    response.Status = false;
                    response.Message = "Current password does not match";
                    return response;
                }

                var logInNewRes = await ValidateLoginCredentials<T>(logInSpName, logInSpParametersClassObject, newPassword);
                if (logInNewRes.Status)
                {
                    response.Status = false;
                    response.Message = "New password cannot be same as current password";
                    return response;
                }

                response = await connections.QuerySingleAsync<AuthResponse<T>>(
                    sql: spName,
                    param: GenricsDynamicParamterMapper(spParametersClassObject),
                    commandType: CommandType.StoredProcedure);

                response.Data = response.Status ? logInRes.Data : default;

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        #endregion Change Password

        #region Forgot Password

        public async Task<AuthResponse> AddForgetPasswordValidator(string spName, object spParametersObject)
        {
            using var connections = Connection;
            AuthResponse response;
            try
            {
                response = await connections.QueryFirstOrDefaultAsync<AuthResponse>(
                        sql: spName,
                        param: GenricsDynamicParamterMapper(spParametersObject),
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public async Task<AuthResponse> ResetPassword(string spName, object spParametersObject)
        {
            using var connections = Connection;
            AuthResponse response = new();
            try
            {
                response = await connections.QueryFirstOrDefaultAsync<AuthResponse>(
                        sql: spName,
                        param: GenricsDynamicParamterMapper(spParametersObject),
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        #endregion Forgot Password

        #region Common Methods

        public async Task<string> GenerateOTP(int length)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var isProd = Convert.ToBoolean(_configuration["IsProduction"]);
                    if (!isProd) return new string(Enumerable.Repeat('1', length).ToArray());
                }
                catch (Exception)
                {
                    // Swallow and return actual otp
                }

                var random = new Random();
                const string chars = "0123456789";
                return new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            });
        }

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            var mySalt = BCryptNet.GenerateSalt();
            passwordSalt = Encoding.ASCII.GetBytes(mySalt);
            passwordHash = Encoding.ASCII.GetBytes(BCryptNet.HashPassword(password, mySalt));
        }

        private static DynamicParameters GenricsDynamicParamterMapper(object tmodelObj)
        {
            var parameter = new DynamicParameters();

            Type tModelType = tmodelObj.GetType();

            //We will be defining a PropertyInfo Object which contains details about the class property
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();

            //Now we will loop in all properties one by one to get value
            foreach (PropertyInfo property in arrayPropertyInfos)
            {
                if (!property.CustomAttributes.Any(x => x.AttributeType.Name.Contains("Ignore")))
                {
                    if (property.PropertyType == typeof(DataTable))
                    {
                        parameter.Add(string.Concat("@", property.Name), ((DataTable)property.GetValue(tmodelObj)).AsTableValuedParameter());
                    }
                    else
                    {
                        parameter.Add(string.Concat("@", property.Name), property.GetValue(tmodelObj));
                    }
                }
            }

            return parameter;
        }

        #endregion Common Methods

        public Task<string> GenerateJWTToken(string userId, string email, string role, Dictionary<string, string> additionalClaims = null, DateTime? expiration = null)
        {
            additionalClaims ??= new Dictionary<string, string>();

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["SecretKey"]);

            var claims = new List<Claim>()
                {
                          new Claim(ClaimTypes.Name, userId),
                          new Claim(ClaimTypes.Role, role),
                          new Claim(ClaimTypes.Email, email)
                };

            foreach (var claim in additionalClaims)
            {
                claims.Add(new Claim(claim.Key, claim.Value));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = expiration ?? DateTime.UtcNow.AddDays(20),
                Audience = "",
                Issuer = "",
                IssuedAt = DateTime.UtcNow,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return Task.FromResult(tokenString);
        }
    }
}