﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuthLibrary
{
    public interface IAuthLib
    {
        string AppSettingsKeys(string nodeName, string key);

        IConfigurationSection GetConfigurationSection(string key);

        Task<T> SignUp<T>(string storedProcedureName, object spParametersClassObject);

        Task<AuthResponse<T>> ValidateLoginCredentials<T>(string spName, object spParametersClassObject, string password);

        Task<AuthResponse> Logout(string token, string spName, object spParametersPocoMapper);

        Task<AuthResponse<Tuple<T, TRole>>> Login<T, TRole>(string spName, object spParametersClassObject, string password, bool isTwoFactorAuth);

        Task<AuthResponse> SaveToken(string spName, object spParametersObject);

        Task<string> GenerateOTP(int length);

        Task<AuthResponse> VerifyOneTimePassword(string spName, object spParametersClassObject);

        Task<AuthResponse<T>> ChangePassword<T>(string spName, object spParametersClassObject, string logInSpName, object logInSpParametersClassObject, string currentPassword, string newPassword);

        Task<AuthResponse> AddForgetPasswordValidator(string spName, object spParametersObject);

        Task<AuthResponse> ResetPassword(string spName, object spParametersObject);

        Task<string> GenerateJWTToken(string userId, string email, string role, Dictionary<string, string> additionalClaims = null, DateTime? expiration = null);
    }
}