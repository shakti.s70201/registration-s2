﻿using System.Collections.Generic;

namespace AuthLibrary
{
    public class AuthResponse
    {
        public bool Status
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }
    }

    public class AuthResponse<T> : AuthResponse
    {
        public T Data
        {
            get; set;
        }
    }

    public class AuthResponseList<T> : AuthResponse
    {
        public List<T> Data
        {
            get; set;
        }
    }

    public class AuthPagedResponseList<T> : AuthResponse
    {
        public List<T> Data { get; set; }

        public int RecordsTotal { get; set; }

        public string Draw { get; set; }
    }
}